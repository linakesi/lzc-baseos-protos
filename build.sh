#!/bin/bash
set -e

IMAGE=registry.corp.lazycat.cloud/lzc/lzc-api-protoc

docker run -u "$UID" -ti -w "$PWD" -v "$PWD:$PWD" --rm "$IMAGE" ./tools/proto-build.sh

(
  cd ./lang/go
  go mod tidy
  go list ./... | xargs go build
)
