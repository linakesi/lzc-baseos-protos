#!/bin/bash
set -e

SHELLDIR="$(dirname "$(readlink -f "$0")")"
cd "$SHELLDIR/.."

# shellcheck disable=SC2207
proto_files=($(find ./baseos -name "*.proto"))

rm -rf ./lang/js/
mkdir -p ./lang/js
${PROTOC:-protoc} -I . \
		  --ts_proto_out=./lang/js \
		  --ts_proto_opt=outputClientImpl=grpc-web \
		  --ts_proto_opt=exportCommonSymbols=false \
		  --ts_proto_opt=esModuleInterop=true \
		  --ts_proto_opt=useAbortSignal=true \
		  "${proto_files[@]}"

mkdir -p ./lang/go
find ./lang/go -name '*.pb.go' -delete
${PROTOC:-protoc} -I . \
 		  --go_out=./lang/go \
 		  --go-grpc_out=./lang/go \
 		  --go_opt=paths=source_relative \
 		  --go-grpc_opt=paths=source_relative \
		  "${proto_files[@]}"
