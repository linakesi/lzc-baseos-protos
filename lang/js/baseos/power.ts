/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import _m0 from "protobufjs/minimal";
import { Empty } from "../google/protobuf/empty";

export interface ShutdownRequest {
  action: ShutdownRequest_Action;
}

export enum ShutdownRequest_Action {
  /** Poweroff - 关机 */
  Poweroff = 0,
  /** Reboot - 重启 */
  Reboot = 1,
  /**
   * SoftReboot - 软重启（仅重启 lzc-os 容器，不关闭 lzc-base-os）
   *    通常用于系统更新、故障修复等场景
   */
  SoftReboot = 2,
  UNRECOGNIZED = -1,
}

export function shutdownRequest_ActionFromJSON(object: any): ShutdownRequest_Action {
  switch (object) {
    case 0:
    case "Poweroff":
      return ShutdownRequest_Action.Poweroff;
    case 1:
    case "Reboot":
      return ShutdownRequest_Action.Reboot;
    case 2:
    case "SoftReboot":
      return ShutdownRequest_Action.SoftReboot;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ShutdownRequest_Action.UNRECOGNIZED;
  }
}

export function shutdownRequest_ActionToJSON(object: ShutdownRequest_Action): string {
  switch (object) {
    case ShutdownRequest_Action.Poweroff:
      return "Poweroff";
    case ShutdownRequest_Action.Reboot:
      return "Reboot";
    case ShutdownRequest_Action.SoftReboot:
      return "SoftReboot";
    case ShutdownRequest_Action.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseShutdownRequest(): ShutdownRequest {
  return { action: 0 };
}

export const ShutdownRequest = {
  encode(message: ShutdownRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.action !== 0) {
      writer.uint32(8).int32(message.action);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ShutdownRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseShutdownRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.action = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ShutdownRequest {
    return { action: isSet(object.action) ? shutdownRequest_ActionFromJSON(object.action) : 0 };
  },

  toJSON(message: ShutdownRequest): unknown {
    const obj: any = {};
    if (message.action !== 0) {
      obj.action = shutdownRequest_ActionToJSON(message.action);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ShutdownRequest>, I>>(base?: I): ShutdownRequest {
    return ShutdownRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ShutdownRequest>, I>>(object: I): ShutdownRequest {
    const message = createBaseShutdownRequest();
    message.action = object.action ?? 0;
    return message;
  },
};

export interface PowerService {
  /** 关机 */
  Shutdown(request: DeepPartial<ShutdownRequest>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
}

export class PowerServiceClientImpl implements PowerService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Shutdown = this.Shutdown.bind(this);
  }

  Shutdown(request: DeepPartial<ShutdownRequest>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(PowerServiceShutdownDesc, ShutdownRequest.fromPartial(request), metadata, abortSignal);
  }
}

export const PowerServiceDesc = { serviceName: "cloud.lazycat.boxservice.daemon.PowerService" };

export const PowerServiceShutdownDesc: UnaryMethodDefinitionish = {
  methodName: "Shutdown",
  service: PowerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ShutdownRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
