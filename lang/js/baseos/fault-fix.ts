/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Empty } from "../google/protobuf/empty";

export interface BootOption {
  type: BootOption_BootOptionType;
}

export enum BootOption_BootOptionType {
  /** BOOT_OPTION_NONE - 无操作（清空所有已设置的操作） */
  BOOT_OPTION_NONE = 0,
  /** BOOT_OPTION_ROLLBACK - 回滚到上一个版本 */
  BOOT_OPTION_ROLLBACK = 1,
  /** BOOT_OPTION_RESET - 重置系统（清空系统的 var 数据） */
  BOOT_OPTION_RESET = 2,
  /** BOOT_OPTION_FACTORY_RESET - 恢复出厂设置（清空用户信息、系统的 var 数据和用户数据） */
  BOOT_OPTION_FACTORY_RESET = 3,
  /** BOOT_OPTION_BACKUP_RESTORE - 备份还原（回滚到目录下的放置的版本） */
  BOOT_OPTION_BACKUP_RESTORE = 4,
  UNRECOGNIZED = -1,
}

export function bootOption_BootOptionTypeFromJSON(object: any): BootOption_BootOptionType {
  switch (object) {
    case 0:
    case "BOOT_OPTION_NONE":
      return BootOption_BootOptionType.BOOT_OPTION_NONE;
    case 1:
    case "BOOT_OPTION_ROLLBACK":
      return BootOption_BootOptionType.BOOT_OPTION_ROLLBACK;
    case 2:
    case "BOOT_OPTION_RESET":
      return BootOption_BootOptionType.BOOT_OPTION_RESET;
    case 3:
    case "BOOT_OPTION_FACTORY_RESET":
      return BootOption_BootOptionType.BOOT_OPTION_FACTORY_RESET;
    case 4:
    case "BOOT_OPTION_BACKUP_RESTORE":
      return BootOption_BootOptionType.BOOT_OPTION_BACKUP_RESTORE;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BootOption_BootOptionType.UNRECOGNIZED;
  }
}

export function bootOption_BootOptionTypeToJSON(object: BootOption_BootOptionType): string {
  switch (object) {
    case BootOption_BootOptionType.BOOT_OPTION_NONE:
      return "BOOT_OPTION_NONE";
    case BootOption_BootOptionType.BOOT_OPTION_ROLLBACK:
      return "BOOT_OPTION_ROLLBACK";
    case BootOption_BootOptionType.BOOT_OPTION_RESET:
      return "BOOT_OPTION_RESET";
    case BootOption_BootOptionType.BOOT_OPTION_FACTORY_RESET:
      return "BOOT_OPTION_FACTORY_RESET";
    case BootOption_BootOptionType.BOOT_OPTION_BACKUP_RESTORE:
      return "BOOT_OPTION_BACKUP_RESTORE";
    case BootOption_BootOptionType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface UploadLogOption {
  url: string;
}

export interface UploadLogId {
  id: number;
}

export interface UploadBoxLogOption {
  url: string;
  method: string;
  headers: { [key: string]: string };
}

export interface UploadBoxLogOption_HeadersEntry {
  key: string;
  value: string;
}

function createBaseBootOption(): BootOption {
  return { type: 0 };
}

export const BootOption = {
  encode(message: BootOption, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.type !== 0) {
      writer.uint32(8).int32(message.type);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BootOption {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBootOption();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.type = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BootOption {
    return { type: isSet(object.type) ? bootOption_BootOptionTypeFromJSON(object.type) : 0 };
  },

  toJSON(message: BootOption): unknown {
    const obj: any = {};
    if (message.type !== 0) {
      obj.type = bootOption_BootOptionTypeToJSON(message.type);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BootOption>, I>>(base?: I): BootOption {
    return BootOption.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BootOption>, I>>(object: I): BootOption {
    const message = createBaseBootOption();
    message.type = object.type ?? 0;
    return message;
  },
};

function createBaseUploadLogOption(): UploadLogOption {
  return { url: "" };
}

export const UploadLogOption = {
  encode(message: UploadLogOption, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.url !== "") {
      writer.uint32(10).string(message.url);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadLogOption {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadLogOption();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.url = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UploadLogOption {
    return { url: isSet(object.url) ? String(object.url) : "" };
  },

  toJSON(message: UploadLogOption): unknown {
    const obj: any = {};
    if (message.url !== "") {
      obj.url = message.url;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadLogOption>, I>>(base?: I): UploadLogOption {
    return UploadLogOption.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadLogOption>, I>>(object: I): UploadLogOption {
    const message = createBaseUploadLogOption();
    message.url = object.url ?? "";
    return message;
  },
};

function createBaseUploadLogId(): UploadLogId {
  return { id: 0 };
}

export const UploadLogId = {
  encode(message: UploadLogId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int64(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadLogId {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadLogId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.id = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UploadLogId {
    return { id: isSet(object.id) ? Number(object.id) : 0 };
  },

  toJSON(message: UploadLogId): unknown {
    const obj: any = {};
    if (message.id !== 0) {
      obj.id = Math.round(message.id);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadLogId>, I>>(base?: I): UploadLogId {
    return UploadLogId.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadLogId>, I>>(object: I): UploadLogId {
    const message = createBaseUploadLogId();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseUploadBoxLogOption(): UploadBoxLogOption {
  return { url: "", method: "", headers: {} };
}

export const UploadBoxLogOption = {
  encode(message: UploadBoxLogOption, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.url !== "") {
      writer.uint32(10).string(message.url);
    }
    if (message.method !== "") {
      writer.uint32(18).string(message.method);
    }
    Object.entries(message.headers).forEach(([key, value]) => {
      UploadBoxLogOption_HeadersEntry.encode({ key: key as any, value }, writer.uint32(26).fork()).ldelim();
    });
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadBoxLogOption {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadBoxLogOption();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.url = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.method = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          const entry3 = UploadBoxLogOption_HeadersEntry.decode(reader, reader.uint32());
          if (entry3.value !== undefined) {
            message.headers[entry3.key] = entry3.value;
          }
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UploadBoxLogOption {
    return {
      url: isSet(object.url) ? String(object.url) : "",
      method: isSet(object.method) ? String(object.method) : "",
      headers: isObject(object.headers)
        ? Object.entries(object.headers).reduce<{ [key: string]: string }>((acc, [key, value]) => {
          acc[key] = String(value);
          return acc;
        }, {})
        : {},
    };
  },

  toJSON(message: UploadBoxLogOption): unknown {
    const obj: any = {};
    if (message.url !== "") {
      obj.url = message.url;
    }
    if (message.method !== "") {
      obj.method = message.method;
    }
    if (message.headers) {
      const entries = Object.entries(message.headers);
      if (entries.length > 0) {
        obj.headers = {};
        entries.forEach(([k, v]) => {
          obj.headers[k] = v;
        });
      }
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadBoxLogOption>, I>>(base?: I): UploadBoxLogOption {
    return UploadBoxLogOption.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadBoxLogOption>, I>>(object: I): UploadBoxLogOption {
    const message = createBaseUploadBoxLogOption();
    message.url = object.url ?? "";
    message.method = object.method ?? "";
    message.headers = Object.entries(object.headers ?? {}).reduce<{ [key: string]: string }>((acc, [key, value]) => {
      if (value !== undefined) {
        acc[key] = String(value);
      }
      return acc;
    }, {});
    return message;
  },
};

function createBaseUploadBoxLogOption_HeadersEntry(): UploadBoxLogOption_HeadersEntry {
  return { key: "", value: "" };
}

export const UploadBoxLogOption_HeadersEntry = {
  encode(message: UploadBoxLogOption_HeadersEntry, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.key !== "") {
      writer.uint32(10).string(message.key);
    }
    if (message.value !== "") {
      writer.uint32(18).string(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadBoxLogOption_HeadersEntry {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadBoxLogOption_HeadersEntry();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.key = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.value = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UploadBoxLogOption_HeadersEntry {
    return { key: isSet(object.key) ? String(object.key) : "", value: isSet(object.value) ? String(object.value) : "" };
  },

  toJSON(message: UploadBoxLogOption_HeadersEntry): unknown {
    const obj: any = {};
    if (message.key !== "") {
      obj.key = message.key;
    }
    if (message.value !== "") {
      obj.value = message.value;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadBoxLogOption_HeadersEntry>, I>>(base?: I): UploadBoxLogOption_HeadersEntry {
    return UploadBoxLogOption_HeadersEntry.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadBoxLogOption_HeadersEntry>, I>>(
    object: I,
  ): UploadBoxLogOption_HeadersEntry {
    const message = createBaseUploadBoxLogOption_HeadersEntry();
    message.key = object.key ?? "";
    message.value = object.value ?? "";
    return message;
  },
};

export interface FaultFixService {
  SetBootOption(request: DeepPartial<BootOption>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** deprecated */
  UploadLog(
    request: DeepPartial<UploadLogOption>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UploadLogId>;
  UploadBoxLog(
    request: DeepPartial<UploadBoxLogOption>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
}

export class FaultFixServiceClientImpl implements FaultFixService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.SetBootOption = this.SetBootOption.bind(this);
    this.UploadLog = this.UploadLog.bind(this);
    this.UploadBoxLog = this.UploadBoxLog.bind(this);
  }

  SetBootOption(request: DeepPartial<BootOption>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(FaultFixServiceSetBootOptionDesc, BootOption.fromPartial(request), metadata, abortSignal);
  }

  UploadLog(
    request: DeepPartial<UploadLogOption>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UploadLogId> {
    return this.rpc.unary(FaultFixServiceUploadLogDesc, UploadLogOption.fromPartial(request), metadata, abortSignal);
  }

  UploadBoxLog(
    request: DeepPartial<UploadBoxLogOption>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      FaultFixServiceUploadBoxLogDesc,
      UploadBoxLogOption.fromPartial(request),
      metadata,
      abortSignal,
    );
  }
}

export const FaultFixServiceDesc = { serviceName: "cloud.lazycat.boxservice.daemon.FaultFixService" };

export const FaultFixServiceSetBootOptionDesc: UnaryMethodDefinitionish = {
  methodName: "SetBootOption",
  service: FaultFixServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return BootOption.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const FaultFixServiceUploadLogDesc: UnaryMethodDefinitionish = {
  methodName: "UploadLog",
  service: FaultFixServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UploadLogOption.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = UploadLogId.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const FaultFixServiceUploadBoxLogDesc: UnaryMethodDefinitionish = {
  methodName: "UploadBoxLog",
  service: FaultFixServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UploadBoxLogOption.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isObject(value: any): boolean {
  return typeof value === "object" && value !== null;
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
