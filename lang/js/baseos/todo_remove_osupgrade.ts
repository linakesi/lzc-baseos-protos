/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Empty } from "../google/protobuf/empty";

export enum DownloaderStatus {
  ready = 0,
  downloading = 1,
  paused = 2,
  completed = 3,
  error = 4,
  UNRECOGNIZED = -1,
}

export function downloaderStatusFromJSON(object: any): DownloaderStatus {
  switch (object) {
    case 0:
    case "ready":
      return DownloaderStatus.ready;
    case 1:
    case "downloading":
      return DownloaderStatus.downloading;
    case 2:
    case "paused":
      return DownloaderStatus.paused;
    case 3:
    case "completed":
      return DownloaderStatus.completed;
    case 4:
    case "error":
      return DownloaderStatus.error;
    case -1:
    case "UNRECOGNIZED":
    default:
      return DownloaderStatus.UNRECOGNIZED;
  }
}

export function downloaderStatusToJSON(object: DownloaderStatus): string {
  switch (object) {
    case DownloaderStatus.ready:
      return "ready";
    case DownloaderStatus.downloading:
      return "downloading";
    case DownloaderStatus.paused:
      return "paused";
    case DownloaderStatus.completed:
      return "completed";
    case DownloaderStatus.error:
      return "error";
    case DownloaderStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface LatestRemoteRequest {
  releaseType: LatestRemoteRequest_ReleaseType;
}

export enum LatestRemoteRequest_ReleaseType {
  Stable = 0,
  Testing = 1,
  UNRECOGNIZED = -1,
}

export function latestRemoteRequest_ReleaseTypeFromJSON(object: any): LatestRemoteRequest_ReleaseType {
  switch (object) {
    case 0:
    case "Stable":
      return LatestRemoteRequest_ReleaseType.Stable;
    case 1:
    case "Testing":
      return LatestRemoteRequest_ReleaseType.Testing;
    case -1:
    case "UNRECOGNIZED":
    default:
      return LatestRemoteRequest_ReleaseType.UNRECOGNIZED;
  }
}

export function latestRemoteRequest_ReleaseTypeToJSON(object: LatestRemoteRequest_ReleaseType): string {
  switch (object) {
    case LatestRemoteRequest_ReleaseType.Stable:
      return "Stable";
    case LatestRemoteRequest_ReleaseType.Testing:
      return "Testing";
    case LatestRemoteRequest_ReleaseType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface GetLatestRequest {
  groups: string[];
}

export interface SystemVersionInfo {
  /** 系统版本 */
  version: string;
  /** ChangeLog */
  changelog: string;
  /** 大小（字节） */
  size: number;
}

export interface UpgradeProgressInfo {
  /** 正在下载的系统版本，（为空表示没有正在下载的进度，后面所有字段都没有意义） */
  version: string;
  status: DownloaderStatus;
  /** 已下载的大小（字节） */
  complete: number;
  /** 总需要下载的大小（字节） */
  total: number;
  /** 错误信息（在没有错误时该值为空） */
  error: string;
}

export interface SystemVersion {
  version: string;
}

export interface UpdateStatus {
  /** 是否正在安装 */
  installing: boolean;
  /** 错误信息（在没有错误时无该值） */
  error?: string | undefined;
}

function createBaseLatestRemoteRequest(): LatestRemoteRequest {
  return { releaseType: 0 };
}

export const LatestRemoteRequest = {
  encode(message: LatestRemoteRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.releaseType !== 0) {
      writer.uint32(8).int32(message.releaseType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LatestRemoteRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLatestRemoteRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.releaseType = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LatestRemoteRequest {
    return { releaseType: isSet(object.releaseType) ? latestRemoteRequest_ReleaseTypeFromJSON(object.releaseType) : 0 };
  },

  toJSON(message: LatestRemoteRequest): unknown {
    const obj: any = {};
    if (message.releaseType !== 0) {
      obj.releaseType = latestRemoteRequest_ReleaseTypeToJSON(message.releaseType);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LatestRemoteRequest>, I>>(base?: I): LatestRemoteRequest {
    return LatestRemoteRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LatestRemoteRequest>, I>>(object: I): LatestRemoteRequest {
    const message = createBaseLatestRemoteRequest();
    message.releaseType = object.releaseType ?? 0;
    return message;
  },
};

function createBaseGetLatestRequest(): GetLatestRequest {
  return { groups: [] };
}

export const GetLatestRequest = {
  encode(message: GetLatestRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.groups) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetLatestRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetLatestRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.groups.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetLatestRequest {
    return { groups: Array.isArray(object?.groups) ? object.groups.map((e: any) => String(e)) : [] };
  },

  toJSON(message: GetLatestRequest): unknown {
    const obj: any = {};
    if (message.groups?.length) {
      obj.groups = message.groups;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetLatestRequest>, I>>(base?: I): GetLatestRequest {
    return GetLatestRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetLatestRequest>, I>>(object: I): GetLatestRequest {
    const message = createBaseGetLatestRequest();
    message.groups = object.groups?.map((e) => e) || [];
    return message;
  },
};

function createBaseSystemVersionInfo(): SystemVersionInfo {
  return { version: "", changelog: "", size: 0 };
}

export const SystemVersionInfo = {
  encode(message: SystemVersionInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== "") {
      writer.uint32(10).string(message.version);
    }
    if (message.changelog !== "") {
      writer.uint32(18).string(message.changelog);
    }
    if (message.size !== 0) {
      writer.uint32(24).uint64(message.size);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemVersionInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSystemVersionInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.version = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.changelog = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.size = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SystemVersionInfo {
    return {
      version: isSet(object.version) ? String(object.version) : "",
      changelog: isSet(object.changelog) ? String(object.changelog) : "",
      size: isSet(object.size) ? Number(object.size) : 0,
    };
  },

  toJSON(message: SystemVersionInfo): unknown {
    const obj: any = {};
    if (message.version !== "") {
      obj.version = message.version;
    }
    if (message.changelog !== "") {
      obj.changelog = message.changelog;
    }
    if (message.size !== 0) {
      obj.size = Math.round(message.size);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SystemVersionInfo>, I>>(base?: I): SystemVersionInfo {
    return SystemVersionInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SystemVersionInfo>, I>>(object: I): SystemVersionInfo {
    const message = createBaseSystemVersionInfo();
    message.version = object.version ?? "";
    message.changelog = object.changelog ?? "";
    message.size = object.size ?? 0;
    return message;
  },
};

function createBaseUpgradeProgressInfo(): UpgradeProgressInfo {
  return { version: "", status: 0, complete: 0, total: 0, error: "" };
}

export const UpgradeProgressInfo = {
  encode(message: UpgradeProgressInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== "") {
      writer.uint32(10).string(message.version);
    }
    if (message.status !== 0) {
      writer.uint32(16).int32(message.status);
    }
    if (message.complete !== 0) {
      writer.uint32(24).int64(message.complete);
    }
    if (message.total !== 0) {
      writer.uint32(32).int64(message.total);
    }
    if (message.error !== "") {
      writer.uint32(42).string(message.error);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpgradeProgressInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpgradeProgressInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.version = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.complete = longToNumber(reader.int64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.total = longToNumber(reader.int64() as Long);
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.error = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UpgradeProgressInfo {
    return {
      version: isSet(object.version) ? String(object.version) : "",
      status: isSet(object.status) ? downloaderStatusFromJSON(object.status) : 0,
      complete: isSet(object.complete) ? Number(object.complete) : 0,
      total: isSet(object.total) ? Number(object.total) : 0,
      error: isSet(object.error) ? String(object.error) : "",
    };
  },

  toJSON(message: UpgradeProgressInfo): unknown {
    const obj: any = {};
    if (message.version !== "") {
      obj.version = message.version;
    }
    if (message.status !== 0) {
      obj.status = downloaderStatusToJSON(message.status);
    }
    if (message.complete !== 0) {
      obj.complete = Math.round(message.complete);
    }
    if (message.total !== 0) {
      obj.total = Math.round(message.total);
    }
    if (message.error !== "") {
      obj.error = message.error;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UpgradeProgressInfo>, I>>(base?: I): UpgradeProgressInfo {
    return UpgradeProgressInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UpgradeProgressInfo>, I>>(object: I): UpgradeProgressInfo {
    const message = createBaseUpgradeProgressInfo();
    message.version = object.version ?? "";
    message.status = object.status ?? 0;
    message.complete = object.complete ?? 0;
    message.total = object.total ?? 0;
    message.error = object.error ?? "";
    return message;
  },
};

function createBaseSystemVersion(): SystemVersion {
  return { version: "" };
}

export const SystemVersion = {
  encode(message: SystemVersion, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== "") {
      writer.uint32(10).string(message.version);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemVersion {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSystemVersion();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.version = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SystemVersion {
    return { version: isSet(object.version) ? String(object.version) : "" };
  },

  toJSON(message: SystemVersion): unknown {
    const obj: any = {};
    if (message.version !== "") {
      obj.version = message.version;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SystemVersion>, I>>(base?: I): SystemVersion {
    return SystemVersion.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SystemVersion>, I>>(object: I): SystemVersion {
    const message = createBaseSystemVersion();
    message.version = object.version ?? "";
    return message;
  },
};

function createBaseUpdateStatus(): UpdateStatus {
  return { installing: false, error: undefined };
}

export const UpdateStatus = {
  encode(message: UpdateStatus, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.installing === true) {
      writer.uint32(8).bool(message.installing);
    }
    if (message.error !== undefined) {
      writer.uint32(18).string(message.error);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateStatus {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUpdateStatus();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.installing = reader.bool();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.error = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UpdateStatus {
    return {
      installing: isSet(object.installing) ? Boolean(object.installing) : false,
      error: isSet(object.error) ? String(object.error) : undefined,
    };
  },

  toJSON(message: UpdateStatus): unknown {
    const obj: any = {};
    if (message.installing === true) {
      obj.installing = message.installing;
    }
    if (message.error !== undefined) {
      obj.error = message.error;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UpdateStatus>, I>>(base?: I): UpdateStatus {
    return UpdateStatus.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UpdateStatus>, I>>(object: I): UpdateStatus {
    const message = createBaseUpdateStatus();
    message.installing = object.installing ?? false;
    message.error = object.error ?? undefined;
    return message;
  },
};

export interface OSUpgradeService {
  /** 获取当前系统的版本状态 */
  Local(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<SystemVersionInfo>;
  /** 获取指定版本系统信息 */
  Remote(
    request: DeepPartial<SystemVersion>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo>;
  /** 获取指定发行类型的最新系统信息（兼容旧接口，准备删除） */
  LatestRemote(
    request: DeepPartial<LatestRemoteRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo>;
  /** 获取指定发行类型的最新系统信息 */
  GetLatest(
    request: DeepPartial<GetLatestRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo>;
  /** 选择远程某个版本，获取到大小准备下载 */
  Select(request: DeepPartial<SystemVersion>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 直接传入 URLs 然后下载，以后把相关检测逻辑放在客户端 */
  Download(request: DeepPartial<SystemVersion>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 获取下载器当前已选择的版本 */
  GetSelected(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo>;
  /** 开始下载 */
  Start(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 暂停下载 */
  Pause(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 获取下载进度 */
  Progress(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UpgradeProgressInfo>;
  /** 安装当前已下好的版本 */
  Install(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 开始安装（立即返回，会设置状态，最后自动重启） */
  StartUpgrade(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 获取安装状态 */
  GetUpdateStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UpdateStatus>;
  /** 清理（阻塞） */
  Prune(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
}

export class OSUpgradeServiceClientImpl implements OSUpgradeService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Local = this.Local.bind(this);
    this.Remote = this.Remote.bind(this);
    this.LatestRemote = this.LatestRemote.bind(this);
    this.GetLatest = this.GetLatest.bind(this);
    this.Select = this.Select.bind(this);
    this.Download = this.Download.bind(this);
    this.GetSelected = this.GetSelected.bind(this);
    this.Start = this.Start.bind(this);
    this.Pause = this.Pause.bind(this);
    this.Progress = this.Progress.bind(this);
    this.Install = this.Install.bind(this);
    this.StartUpgrade = this.StartUpgrade.bind(this);
    this.GetUpdateStatus = this.GetUpdateStatus.bind(this);
    this.Prune = this.Prune.bind(this);
  }

  Local(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<SystemVersionInfo> {
    return this.rpc.unary(OSUpgradeServiceLocalDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Remote(
    request: DeepPartial<SystemVersion>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo> {
    return this.rpc.unary(OSUpgradeServiceRemoteDesc, SystemVersion.fromPartial(request), metadata, abortSignal);
  }

  LatestRemote(
    request: DeepPartial<LatestRemoteRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo> {
    return this.rpc.unary(
      OSUpgradeServiceLatestRemoteDesc,
      LatestRemoteRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  GetLatest(
    request: DeepPartial<GetLatestRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo> {
    return this.rpc.unary(OSUpgradeServiceGetLatestDesc, GetLatestRequest.fromPartial(request), metadata, abortSignal);
  }

  Select(request: DeepPartial<SystemVersion>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServiceSelectDesc, SystemVersion.fromPartial(request), metadata, abortSignal);
  }

  Download(request: DeepPartial<SystemVersion>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServiceDownloadDesc, SystemVersion.fromPartial(request), metadata, abortSignal);
  }

  GetSelected(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SystemVersionInfo> {
    return this.rpc.unary(OSUpgradeServiceGetSelectedDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Start(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServiceStartDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Pause(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServicePauseDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Progress(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UpgradeProgressInfo> {
    return this.rpc.unary(OSUpgradeServiceProgressDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Install(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServiceInstallDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  StartUpgrade(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServiceStartUpgradeDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  GetUpdateStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UpdateStatus> {
    return this.rpc.unary(OSUpgradeServiceGetUpdateStatusDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Prune(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgradeServicePruneDesc, Empty.fromPartial(request), metadata, abortSignal);
  }
}

export const OSUpgradeServiceDesc = { serviceName: "cloud.lazycat.boxservice.daemon.OSUpgradeService" };

export const OSUpgradeServiceLocalDesc: UnaryMethodDefinitionish = {
  methodName: "Local",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SystemVersionInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceRemoteDesc: UnaryMethodDefinitionish = {
  methodName: "Remote",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SystemVersion.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SystemVersionInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceLatestRemoteDesc: UnaryMethodDefinitionish = {
  methodName: "LatestRemote",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return LatestRemoteRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SystemVersionInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceGetLatestDesc: UnaryMethodDefinitionish = {
  methodName: "GetLatest",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetLatestRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SystemVersionInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceSelectDesc: UnaryMethodDefinitionish = {
  methodName: "Select",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SystemVersion.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceDownloadDesc: UnaryMethodDefinitionish = {
  methodName: "Download",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SystemVersion.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceGetSelectedDesc: UnaryMethodDefinitionish = {
  methodName: "GetSelected",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SystemVersionInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceStartDesc: UnaryMethodDefinitionish = {
  methodName: "Start",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServicePauseDesc: UnaryMethodDefinitionish = {
  methodName: "Pause",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceProgressDesc: UnaryMethodDefinitionish = {
  methodName: "Progress",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = UpgradeProgressInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceInstallDesc: UnaryMethodDefinitionish = {
  methodName: "Install",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceStartUpgradeDesc: UnaryMethodDefinitionish = {
  methodName: "StartUpgrade",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServiceGetUpdateStatusDesc: UnaryMethodDefinitionish = {
  methodName: "GetUpdateStatus",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = UpdateStatus.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgradeServicePruneDesc: UnaryMethodDefinitionish = {
  methodName: "Prune",
  service: OSUpgradeServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
