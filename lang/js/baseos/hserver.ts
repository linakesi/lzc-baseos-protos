/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../google/protobuf/empty";
import { Timestamp } from "../google/protobuf/timestamp";

export enum DialerRole {
  /** GUEST_USER - 未登录也可以调用 */
  GUEST_USER = 0,
  /** NORMAL_USER - 登录的普通用户可以调用 */
  NORMAL_USER = 1,
  /** ADMIN_USER - 登录的管理员可以调用 */
  ADMIN_USER = 2,
  UNRECOGNIZED = -1,
}

export function dialerRoleFromJSON(object: any): DialerRole {
  switch (object) {
    case 0:
    case "GUEST_USER":
      return DialerRole.GUEST_USER;
    case 1:
    case "NORMAL_USER":
      return DialerRole.NORMAL_USER;
    case 2:
    case "ADMIN_USER":
      return DialerRole.ADMIN_USER;
    case -1:
    case "UNRECOGNIZED":
    default:
      return DialerRole.UNRECOGNIZED;
  }
}

export function dialerRoleToJSON(object: DialerRole): string {
  switch (object) {
    case DialerRole.GUEST_USER:
      return "GUEST_USER";
    case DialerRole.NORMAL_USER:
      return "NORMAL_USER";
    case DialerRole.ADMIN_USER:
      return "ADMIN_USER";
    case DialerRole.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum Role {
  NORMAL = 0,
  ADMIN = 1,
  UNRECOGNIZED = -1,
}

export function roleFromJSON(object: any): Role {
  switch (object) {
    case 0:
    case "NORMAL":
      return Role.NORMAL;
    case 1:
    case "ADMIN":
      return Role.ADMIN;
    case -1:
    case "UNRECOGNIZED":
    default:
      return Role.UNRECOGNIZED;
  }
}

export function roleToJSON(object: Role): string {
  switch (object) {
    case Role.NORMAL:
      return "NORMAL";
    case Role.ADMIN:
      return "ADMIN";
    case Role.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface RegisterBoxServiceRequest {
  /**
   * 服务名称，建议使用 xx.yy.zz.nn 这种形式
   * 如果此服务处于注册中，则后续请求会直接失败结束
   */
  serviceName: string;
  /** tcp、udp */
  serviceNetwork: string;
  /** $ip:$port */
  serviceAddress: string;
  /** 服务的自描述信息 */
  description: string;
  /** 需要调用者的身份 */
  requireDialerRole: DialerRole;
  /** 只有客户端是受信任设备时可以调用 */
  requireTrustDev: boolean;
}

export interface RegisterBoxServiceReply {
}

export interface EmitBoxServiceChangedRequest {
  serviceName: string;
}

export interface QueryBoxServicePeerCredRequest {
  remoteAddr: string;
  localAddr: string;
  protocol: string;
}

export interface QueryBoxServicePeerCredResponse {
  /** 若客户端未登录，则是SRP认证的UID，否则是登录的UID */
  uid: string;
  deviceVersion: string;
  virtualIp: string;
  peerId: string;
  isLogined: boolean;
  isTrustDev: boolean;
  /** dialer实际进来的地址 */
  remoteMultiAddr: string;
}

export interface RemoteSocksRequest {
  locationType: RemoteSocksRequest_LocationType;
  /** peer ID */
  target: string;
}

export enum RemoteSocksRequest_LocationType {
  Unkonwn = 0,
  /** Local - 在当前hserver所处的物理网络协议栈上提供socks服务 */
  Local = 1,
  /** Remote - 在指定的的hclient所处的物理网络协议栈上提供socks服务 */
  Remote = 2,
  UNRECOGNIZED = -1,
}

export function remoteSocksRequest_LocationTypeFromJSON(object: any): RemoteSocksRequest_LocationType {
  switch (object) {
    case 0:
    case "Unkonwn":
      return RemoteSocksRequest_LocationType.Unkonwn;
    case 1:
    case "Local":
      return RemoteSocksRequest_LocationType.Local;
    case 2:
    case "Remote":
      return RemoteSocksRequest_LocationType.Remote;
    case -1:
    case "UNRECOGNIZED":
    default:
      return RemoteSocksRequest_LocationType.UNRECOGNIZED;
  }
}

export function remoteSocksRequest_LocationTypeToJSON(object: RemoteSocksRequest_LocationType): string {
  switch (object) {
    case RemoteSocksRequest_LocationType.Unkonwn:
      return "Unkonwn";
    case RemoteSocksRequest_LocationType.Local:
      return "Local";
    case RemoteSocksRequest_LocationType.Remote:
      return "Remote";
    case RemoteSocksRequest_LocationType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface RemoteSocksReply {
  /** 返回的socks5服务器地址 */
  serverUrl: string;
}

export interface ListUsersReply {
  uids: string[];
}

export interface HServerInfo {
  /** 中心化服务器地址, 默认为origin.lazycat.cloud */
  originServer: string;
  /** fc03:1136:38/40地址 */
  virtualIp: string;
  /** p2p节点id */
  boxId: string;
  /** 注册到origin-server内的名称 */
  boxName: string;
  /** 从origin-server获取到的域名 */
  boxDomain: string;
  /** 当前使用的中继服务器地址，如/ip4/x.x.x.x/tcp/5500 */
  relayServer: string;
}

export interface Device {
  peerId: string;
  /** 此设备当前是否连接到盒子 */
  isOnline: boolean;
  /** 设备绑定时间 */
  bindingTime: Date | undefined;
}

export interface ListEndDeviceRequest {
  uid: string;
}

export interface ListEndDeviceReply {
  devices: Device[];
}

export interface UserID {
  uid: string;
}

export interface ChangeRoleReqeust {
  uid: string;
  role: Role;
}

export interface QueryRoleReply {
  uid: string;
  role: Role;
}

export interface ResetPasswordRequest {
  uid: string;
  newPassword: string;
}

export interface DeleteUserRequest {
  uid: string;
}

export interface CreateUserRequest {
  uid: string;
  password: string;
  role: Role;
}

export interface CheckPasswordRequest {
  uid: string;
  password: string;
}

export interface ChangeTrustEndDeviceRequest {
  uid: string;
  peerId: string;
  action: ChangeTrustEndDeviceRequest_Action;
}

export enum ChangeTrustEndDeviceRequest_Action {
  Add = 0,
  Del = 1,
  UNRECOGNIZED = -1,
}

export function changeTrustEndDeviceRequest_ActionFromJSON(object: any): ChangeTrustEndDeviceRequest_Action {
  switch (object) {
    case 0:
    case "Add":
      return ChangeTrustEndDeviceRequest_Action.Add;
    case 1:
    case "Del":
      return ChangeTrustEndDeviceRequest_Action.Del;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ChangeTrustEndDeviceRequest_Action.UNRECOGNIZED;
  }
}

export function changeTrustEndDeviceRequest_ActionToJSON(object: ChangeTrustEndDeviceRequest_Action): string {
  switch (object) {
    case ChangeTrustEndDeviceRequest_Action.Add:
      return "Add";
    case ChangeTrustEndDeviceRequest_Action.Del:
      return "Del";
    case ChangeTrustEndDeviceRequest_Action.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface ChangeTrustEndDeviceReply {
}

export interface SetupHServerRequest {
  originServer: string;
  boxName: string;
  username: string;
  password: string;
}

export interface SetupHServerReply {
  ok: boolean;
  reason: string;
}

export interface ResetHServerRequest {
  originServer: string;
}

export interface ResetHServerReply {
}

export interface PeersInfo {
  peerInfo: string;
}

export interface GetPasswordHashRequest {
  uid: string;
}

export interface GetPasswordHashResponse {
  passwordHash: string;
}

export interface SetPasswordHashRequest {
  uid: string;
  passwordHash: string;
}

export interface SetRelayRequest {
  relayAddress: string;
}

export interface SetRelaysRequest {
  relayAddresses: string[];
}

export interface AllowProxyItRequest {
  peerId: string;
  /** true=添加、false=删除 */
  disable: boolean;
}

export interface AllowProxyItResponse {
}

export interface Address {
  /** 例如：/ip4/6.6.6.6/tcp/5500 */
  multiaddr: string;
  /** 以秒为单位 */
  ttl: number;
}

export interface AddAddressRequest {
  addresses: Address[];
}

export interface AddAddressResponse {
}

function createBaseRegisterBoxServiceRequest(): RegisterBoxServiceRequest {
  return {
    serviceName: "",
    serviceNetwork: "",
    serviceAddress: "",
    description: "",
    requireDialerRole: 0,
    requireTrustDev: false,
  };
}

export const RegisterBoxServiceRequest = {
  encode(message: RegisterBoxServiceRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.serviceName !== "") {
      writer.uint32(10).string(message.serviceName);
    }
    if (message.serviceNetwork !== "") {
      writer.uint32(18).string(message.serviceNetwork);
    }
    if (message.serviceAddress !== "") {
      writer.uint32(26).string(message.serviceAddress);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.requireDialerRole !== 0) {
      writer.uint32(56).int32(message.requireDialerRole);
    }
    if (message.requireTrustDev === true) {
      writer.uint32(64).bool(message.requireTrustDev);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegisterBoxServiceRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRegisterBoxServiceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.serviceName = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.serviceNetwork = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.serviceAddress = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.description = reader.string();
          continue;
        case 7:
          if (tag !== 56) {
            break;
          }

          message.requireDialerRole = reader.int32() as any;
          continue;
        case 8:
          if (tag !== 64) {
            break;
          }

          message.requireTrustDev = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RegisterBoxServiceRequest {
    return {
      serviceName: isSet(object.serviceName) ? String(object.serviceName) : "",
      serviceNetwork: isSet(object.serviceNetwork) ? String(object.serviceNetwork) : "",
      serviceAddress: isSet(object.serviceAddress) ? String(object.serviceAddress) : "",
      description: isSet(object.description) ? String(object.description) : "",
      requireDialerRole: isSet(object.requireDialerRole) ? dialerRoleFromJSON(object.requireDialerRole) : 0,
      requireTrustDev: isSet(object.requireTrustDev) ? Boolean(object.requireTrustDev) : false,
    };
  },

  toJSON(message: RegisterBoxServiceRequest): unknown {
    const obj: any = {};
    if (message.serviceName !== "") {
      obj.serviceName = message.serviceName;
    }
    if (message.serviceNetwork !== "") {
      obj.serviceNetwork = message.serviceNetwork;
    }
    if (message.serviceAddress !== "") {
      obj.serviceAddress = message.serviceAddress;
    }
    if (message.description !== "") {
      obj.description = message.description;
    }
    if (message.requireDialerRole !== 0) {
      obj.requireDialerRole = dialerRoleToJSON(message.requireDialerRole);
    }
    if (message.requireTrustDev === true) {
      obj.requireTrustDev = message.requireTrustDev;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RegisterBoxServiceRequest>, I>>(base?: I): RegisterBoxServiceRequest {
    return RegisterBoxServiceRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RegisterBoxServiceRequest>, I>>(object: I): RegisterBoxServiceRequest {
    const message = createBaseRegisterBoxServiceRequest();
    message.serviceName = object.serviceName ?? "";
    message.serviceNetwork = object.serviceNetwork ?? "";
    message.serviceAddress = object.serviceAddress ?? "";
    message.description = object.description ?? "";
    message.requireDialerRole = object.requireDialerRole ?? 0;
    message.requireTrustDev = object.requireTrustDev ?? false;
    return message;
  },
};

function createBaseRegisterBoxServiceReply(): RegisterBoxServiceReply {
  return {};
}

export const RegisterBoxServiceReply = {
  encode(_: RegisterBoxServiceReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegisterBoxServiceReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRegisterBoxServiceReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): RegisterBoxServiceReply {
    return {};
  },

  toJSON(_: RegisterBoxServiceReply): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<RegisterBoxServiceReply>, I>>(base?: I): RegisterBoxServiceReply {
    return RegisterBoxServiceReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RegisterBoxServiceReply>, I>>(_: I): RegisterBoxServiceReply {
    const message = createBaseRegisterBoxServiceReply();
    return message;
  },
};

function createBaseEmitBoxServiceChangedRequest(): EmitBoxServiceChangedRequest {
  return { serviceName: "" };
}

export const EmitBoxServiceChangedRequest = {
  encode(message: EmitBoxServiceChangedRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.serviceName !== "") {
      writer.uint32(10).string(message.serviceName);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EmitBoxServiceChangedRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEmitBoxServiceChangedRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.serviceName = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): EmitBoxServiceChangedRequest {
    return { serviceName: isSet(object.serviceName) ? String(object.serviceName) : "" };
  },

  toJSON(message: EmitBoxServiceChangedRequest): unknown {
    const obj: any = {};
    if (message.serviceName !== "") {
      obj.serviceName = message.serviceName;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<EmitBoxServiceChangedRequest>, I>>(base?: I): EmitBoxServiceChangedRequest {
    return EmitBoxServiceChangedRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<EmitBoxServiceChangedRequest>, I>>(object: I): EmitBoxServiceChangedRequest {
    const message = createBaseEmitBoxServiceChangedRequest();
    message.serviceName = object.serviceName ?? "";
    return message;
  },
};

function createBaseQueryBoxServicePeerCredRequest(): QueryBoxServicePeerCredRequest {
  return { remoteAddr: "", localAddr: "", protocol: "" };
}

export const QueryBoxServicePeerCredRequest = {
  encode(message: QueryBoxServicePeerCredRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.remoteAddr !== "") {
      writer.uint32(10).string(message.remoteAddr);
    }
    if (message.localAddr !== "") {
      writer.uint32(18).string(message.localAddr);
    }
    if (message.protocol !== "") {
      writer.uint32(26).string(message.protocol);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryBoxServicePeerCredRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryBoxServicePeerCredRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.remoteAddr = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.localAddr = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.protocol = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): QueryBoxServicePeerCredRequest {
    return {
      remoteAddr: isSet(object.remoteAddr) ? String(object.remoteAddr) : "",
      localAddr: isSet(object.localAddr) ? String(object.localAddr) : "",
      protocol: isSet(object.protocol) ? String(object.protocol) : "",
    };
  },

  toJSON(message: QueryBoxServicePeerCredRequest): unknown {
    const obj: any = {};
    if (message.remoteAddr !== "") {
      obj.remoteAddr = message.remoteAddr;
    }
    if (message.localAddr !== "") {
      obj.localAddr = message.localAddr;
    }
    if (message.protocol !== "") {
      obj.protocol = message.protocol;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<QueryBoxServicePeerCredRequest>, I>>(base?: I): QueryBoxServicePeerCredRequest {
    return QueryBoxServicePeerCredRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<QueryBoxServicePeerCredRequest>, I>>(
    object: I,
  ): QueryBoxServicePeerCredRequest {
    const message = createBaseQueryBoxServicePeerCredRequest();
    message.remoteAddr = object.remoteAddr ?? "";
    message.localAddr = object.localAddr ?? "";
    message.protocol = object.protocol ?? "";
    return message;
  },
};

function createBaseQueryBoxServicePeerCredResponse(): QueryBoxServicePeerCredResponse {
  return {
    uid: "",
    deviceVersion: "",
    virtualIp: "",
    peerId: "",
    isLogined: false,
    isTrustDev: false,
    remoteMultiAddr: "",
  };
}

export const QueryBoxServicePeerCredResponse = {
  encode(message: QueryBoxServicePeerCredResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.deviceVersion !== "") {
      writer.uint32(26).string(message.deviceVersion);
    }
    if (message.virtualIp !== "") {
      writer.uint32(34).string(message.virtualIp);
    }
    if (message.peerId !== "") {
      writer.uint32(42).string(message.peerId);
    }
    if (message.isLogined === true) {
      writer.uint32(48).bool(message.isLogined);
    }
    if (message.isTrustDev === true) {
      writer.uint32(56).bool(message.isTrustDev);
    }
    if (message.remoteMultiAddr !== "") {
      writer.uint32(66).string(message.remoteMultiAddr);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryBoxServicePeerCredResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryBoxServicePeerCredResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.deviceVersion = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.virtualIp = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.peerId = reader.string();
          continue;
        case 6:
          if (tag !== 48) {
            break;
          }

          message.isLogined = reader.bool();
          continue;
        case 7:
          if (tag !== 56) {
            break;
          }

          message.isTrustDev = reader.bool();
          continue;
        case 8:
          if (tag !== 66) {
            break;
          }

          message.remoteMultiAddr = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): QueryBoxServicePeerCredResponse {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      deviceVersion: isSet(object.deviceVersion) ? String(object.deviceVersion) : "",
      virtualIp: isSet(object.virtualIp) ? String(object.virtualIp) : "",
      peerId: isSet(object.peerId) ? String(object.peerId) : "",
      isLogined: isSet(object.isLogined) ? Boolean(object.isLogined) : false,
      isTrustDev: isSet(object.isTrustDev) ? Boolean(object.isTrustDev) : false,
      remoteMultiAddr: isSet(object.remoteMultiAddr) ? String(object.remoteMultiAddr) : "",
    };
  },

  toJSON(message: QueryBoxServicePeerCredResponse): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.deviceVersion !== "") {
      obj.deviceVersion = message.deviceVersion;
    }
    if (message.virtualIp !== "") {
      obj.virtualIp = message.virtualIp;
    }
    if (message.peerId !== "") {
      obj.peerId = message.peerId;
    }
    if (message.isLogined === true) {
      obj.isLogined = message.isLogined;
    }
    if (message.isTrustDev === true) {
      obj.isTrustDev = message.isTrustDev;
    }
    if (message.remoteMultiAddr !== "") {
      obj.remoteMultiAddr = message.remoteMultiAddr;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<QueryBoxServicePeerCredResponse>, I>>(base?: I): QueryBoxServicePeerCredResponse {
    return QueryBoxServicePeerCredResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<QueryBoxServicePeerCredResponse>, I>>(
    object: I,
  ): QueryBoxServicePeerCredResponse {
    const message = createBaseQueryBoxServicePeerCredResponse();
    message.uid = object.uid ?? "";
    message.deviceVersion = object.deviceVersion ?? "";
    message.virtualIp = object.virtualIp ?? "";
    message.peerId = object.peerId ?? "";
    message.isLogined = object.isLogined ?? false;
    message.isTrustDev = object.isTrustDev ?? false;
    message.remoteMultiAddr = object.remoteMultiAddr ?? "";
    return message;
  },
};

function createBaseRemoteSocksRequest(): RemoteSocksRequest {
  return { locationType: 0, target: "" };
}

export const RemoteSocksRequest = {
  encode(message: RemoteSocksRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.locationType !== 0) {
      writer.uint32(8).int32(message.locationType);
    }
    if (message.target !== "") {
      writer.uint32(18).string(message.target);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemoteSocksRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemoteSocksRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.locationType = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.target = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemoteSocksRequest {
    return {
      locationType: isSet(object.locationType) ? remoteSocksRequest_LocationTypeFromJSON(object.locationType) : 0,
      target: isSet(object.target) ? String(object.target) : "",
    };
  },

  toJSON(message: RemoteSocksRequest): unknown {
    const obj: any = {};
    if (message.locationType !== 0) {
      obj.locationType = remoteSocksRequest_LocationTypeToJSON(message.locationType);
    }
    if (message.target !== "") {
      obj.target = message.target;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemoteSocksRequest>, I>>(base?: I): RemoteSocksRequest {
    return RemoteSocksRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemoteSocksRequest>, I>>(object: I): RemoteSocksRequest {
    const message = createBaseRemoteSocksRequest();
    message.locationType = object.locationType ?? 0;
    message.target = object.target ?? "";
    return message;
  },
};

function createBaseRemoteSocksReply(): RemoteSocksReply {
  return { serverUrl: "" };
}

export const RemoteSocksReply = {
  encode(message: RemoteSocksReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.serverUrl !== "") {
      writer.uint32(10).string(message.serverUrl);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemoteSocksReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemoteSocksReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.serverUrl = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemoteSocksReply {
    return { serverUrl: isSet(object.serverUrl) ? String(object.serverUrl) : "" };
  },

  toJSON(message: RemoteSocksReply): unknown {
    const obj: any = {};
    if (message.serverUrl !== "") {
      obj.serverUrl = message.serverUrl;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemoteSocksReply>, I>>(base?: I): RemoteSocksReply {
    return RemoteSocksReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemoteSocksReply>, I>>(object: I): RemoteSocksReply {
    const message = createBaseRemoteSocksReply();
    message.serverUrl = object.serverUrl ?? "";
    return message;
  },
};

function createBaseListUsersReply(): ListUsersReply {
  return { uids: [] };
}

export const ListUsersReply = {
  encode(message: ListUsersReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.uids) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListUsersReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListUsersReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uids.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ListUsersReply {
    return { uids: Array.isArray(object?.uids) ? object.uids.map((e: any) => String(e)) : [] };
  },

  toJSON(message: ListUsersReply): unknown {
    const obj: any = {};
    if (message.uids?.length) {
      obj.uids = message.uids;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ListUsersReply>, I>>(base?: I): ListUsersReply {
    return ListUsersReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ListUsersReply>, I>>(object: I): ListUsersReply {
    const message = createBaseListUsersReply();
    message.uids = object.uids?.map((e) => e) || [];
    return message;
  },
};

function createBaseHServerInfo(): HServerInfo {
  return { originServer: "", virtualIp: "", boxId: "", boxName: "", boxDomain: "", relayServer: "" };
}

export const HServerInfo = {
  encode(message: HServerInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.originServer !== "") {
      writer.uint32(10).string(message.originServer);
    }
    if (message.virtualIp !== "") {
      writer.uint32(18).string(message.virtualIp);
    }
    if (message.boxId !== "") {
      writer.uint32(26).string(message.boxId);
    }
    if (message.boxName !== "") {
      writer.uint32(34).string(message.boxName);
    }
    if (message.boxDomain !== "") {
      writer.uint32(42).string(message.boxDomain);
    }
    if (message.relayServer !== "") {
      writer.uint32(50).string(message.relayServer);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): HServerInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseHServerInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.originServer = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.virtualIp = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.boxId = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.boxName = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.boxDomain = reader.string();
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.relayServer = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): HServerInfo {
    return {
      originServer: isSet(object.originServer) ? String(object.originServer) : "",
      virtualIp: isSet(object.virtualIp) ? String(object.virtualIp) : "",
      boxId: isSet(object.boxId) ? String(object.boxId) : "",
      boxName: isSet(object.boxName) ? String(object.boxName) : "",
      boxDomain: isSet(object.boxDomain) ? String(object.boxDomain) : "",
      relayServer: isSet(object.relayServer) ? String(object.relayServer) : "",
    };
  },

  toJSON(message: HServerInfo): unknown {
    const obj: any = {};
    if (message.originServer !== "") {
      obj.originServer = message.originServer;
    }
    if (message.virtualIp !== "") {
      obj.virtualIp = message.virtualIp;
    }
    if (message.boxId !== "") {
      obj.boxId = message.boxId;
    }
    if (message.boxName !== "") {
      obj.boxName = message.boxName;
    }
    if (message.boxDomain !== "") {
      obj.boxDomain = message.boxDomain;
    }
    if (message.relayServer !== "") {
      obj.relayServer = message.relayServer;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<HServerInfo>, I>>(base?: I): HServerInfo {
    return HServerInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<HServerInfo>, I>>(object: I): HServerInfo {
    const message = createBaseHServerInfo();
    message.originServer = object.originServer ?? "";
    message.virtualIp = object.virtualIp ?? "";
    message.boxId = object.boxId ?? "";
    message.boxName = object.boxName ?? "";
    message.boxDomain = object.boxDomain ?? "";
    message.relayServer = object.relayServer ?? "";
    return message;
  },
};

function createBaseDevice(): Device {
  return { peerId: "", isOnline: false, bindingTime: undefined };
}

export const Device = {
  encode(message: Device, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.peerId !== "") {
      writer.uint32(10).string(message.peerId);
    }
    if (message.isOnline === true) {
      writer.uint32(16).bool(message.isOnline);
    }
    if (message.bindingTime !== undefined) {
      Timestamp.encode(toTimestamp(message.bindingTime), writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Device {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDevice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.peerId = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.isOnline = reader.bool();
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.bindingTime = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Device {
    return {
      peerId: isSet(object.peerId) ? String(object.peerId) : "",
      isOnline: isSet(object.isOnline) ? Boolean(object.isOnline) : false,
      bindingTime: isSet(object.bindingTime) ? fromJsonTimestamp(object.bindingTime) : undefined,
    };
  },

  toJSON(message: Device): unknown {
    const obj: any = {};
    if (message.peerId !== "") {
      obj.peerId = message.peerId;
    }
    if (message.isOnline === true) {
      obj.isOnline = message.isOnline;
    }
    if (message.bindingTime !== undefined) {
      obj.bindingTime = message.bindingTime.toISOString();
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<Device>, I>>(base?: I): Device {
    return Device.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Device>, I>>(object: I): Device {
    const message = createBaseDevice();
    message.peerId = object.peerId ?? "";
    message.isOnline = object.isOnline ?? false;
    message.bindingTime = object.bindingTime ?? undefined;
    return message;
  },
};

function createBaseListEndDeviceRequest(): ListEndDeviceRequest {
  return { uid: "" };
}

export const ListEndDeviceRequest = {
  encode(message: ListEndDeviceRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListEndDeviceRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListEndDeviceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ListEndDeviceRequest {
    return { uid: isSet(object.uid) ? String(object.uid) : "" };
  },

  toJSON(message: ListEndDeviceRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ListEndDeviceRequest>, I>>(base?: I): ListEndDeviceRequest {
    return ListEndDeviceRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ListEndDeviceRequest>, I>>(object: I): ListEndDeviceRequest {
    const message = createBaseListEndDeviceRequest();
    message.uid = object.uid ?? "";
    return message;
  },
};

function createBaseListEndDeviceReply(): ListEndDeviceReply {
  return { devices: [] };
}

export const ListEndDeviceReply = {
  encode(message: ListEndDeviceReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.devices) {
      Device.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListEndDeviceReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListEndDeviceReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.devices.push(Device.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ListEndDeviceReply {
    return { devices: Array.isArray(object?.devices) ? object.devices.map((e: any) => Device.fromJSON(e)) : [] };
  },

  toJSON(message: ListEndDeviceReply): unknown {
    const obj: any = {};
    if (message.devices?.length) {
      obj.devices = message.devices.map((e) => Device.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ListEndDeviceReply>, I>>(base?: I): ListEndDeviceReply {
    return ListEndDeviceReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ListEndDeviceReply>, I>>(object: I): ListEndDeviceReply {
    const message = createBaseListEndDeviceReply();
    message.devices = object.devices?.map((e) => Device.fromPartial(e)) || [];
    return message;
  },
};

function createBaseUserID(): UserID {
  return { uid: "" };
}

export const UserID = {
  encode(message: UserID, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserID {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUserID();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UserID {
    return { uid: isSet(object.uid) ? String(object.uid) : "" };
  },

  toJSON(message: UserID): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UserID>, I>>(base?: I): UserID {
    return UserID.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UserID>, I>>(object: I): UserID {
    const message = createBaseUserID();
    message.uid = object.uid ?? "";
    return message;
  },
};

function createBaseChangeRoleReqeust(): ChangeRoleReqeust {
  return { uid: "", role: 0 };
}

export const ChangeRoleReqeust = {
  encode(message: ChangeRoleReqeust, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.role !== 0) {
      writer.uint32(16).int32(message.role);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangeRoleReqeust {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseChangeRoleReqeust();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.role = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ChangeRoleReqeust {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      role: isSet(object.role) ? roleFromJSON(object.role) : 0,
    };
  },

  toJSON(message: ChangeRoleReqeust): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.role !== 0) {
      obj.role = roleToJSON(message.role);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ChangeRoleReqeust>, I>>(base?: I): ChangeRoleReqeust {
    return ChangeRoleReqeust.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ChangeRoleReqeust>, I>>(object: I): ChangeRoleReqeust {
    const message = createBaseChangeRoleReqeust();
    message.uid = object.uid ?? "";
    message.role = object.role ?? 0;
    return message;
  },
};

function createBaseQueryRoleReply(): QueryRoleReply {
  return { uid: "", role: 0 };
}

export const QueryRoleReply = {
  encode(message: QueryRoleReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.role !== 0) {
      writer.uint32(16).int32(message.role);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryRoleReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryRoleReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.role = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): QueryRoleReply {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      role: isSet(object.role) ? roleFromJSON(object.role) : 0,
    };
  },

  toJSON(message: QueryRoleReply): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.role !== 0) {
      obj.role = roleToJSON(message.role);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<QueryRoleReply>, I>>(base?: I): QueryRoleReply {
    return QueryRoleReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<QueryRoleReply>, I>>(object: I): QueryRoleReply {
    const message = createBaseQueryRoleReply();
    message.uid = object.uid ?? "";
    message.role = object.role ?? 0;
    return message;
  },
};

function createBaseResetPasswordRequest(): ResetPasswordRequest {
  return { uid: "", newPassword: "" };
}

export const ResetPasswordRequest = {
  encode(message: ResetPasswordRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.newPassword !== "") {
      writer.uint32(26).string(message.newPassword);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ResetPasswordRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseResetPasswordRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.newPassword = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ResetPasswordRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      newPassword: isSet(object.newPassword) ? String(object.newPassword) : "",
    };
  },

  toJSON(message: ResetPasswordRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.newPassword !== "") {
      obj.newPassword = message.newPassword;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ResetPasswordRequest>, I>>(base?: I): ResetPasswordRequest {
    return ResetPasswordRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ResetPasswordRequest>, I>>(object: I): ResetPasswordRequest {
    const message = createBaseResetPasswordRequest();
    message.uid = object.uid ?? "";
    message.newPassword = object.newPassword ?? "";
    return message;
  },
};

function createBaseDeleteUserRequest(): DeleteUserRequest {
  return { uid: "" };
}

export const DeleteUserRequest = {
  encode(message: DeleteUserRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteUserRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteUserRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DeleteUserRequest {
    return { uid: isSet(object.uid) ? String(object.uid) : "" };
  },

  toJSON(message: DeleteUserRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DeleteUserRequest>, I>>(base?: I): DeleteUserRequest {
    return DeleteUserRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DeleteUserRequest>, I>>(object: I): DeleteUserRequest {
    const message = createBaseDeleteUserRequest();
    message.uid = object.uid ?? "";
    return message;
  },
};

function createBaseCreateUserRequest(): CreateUserRequest {
  return { uid: "", password: "", role: 0 };
}

export const CreateUserRequest = {
  encode(message: CreateUserRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.role !== 0) {
      writer.uint32(24).int32(message.role);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateUserRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCreateUserRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.password = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.role = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): CreateUserRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      password: isSet(object.password) ? String(object.password) : "",
      role: isSet(object.role) ? roleFromJSON(object.role) : 0,
    };
  },

  toJSON(message: CreateUserRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.role !== 0) {
      obj.role = roleToJSON(message.role);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<CreateUserRequest>, I>>(base?: I): CreateUserRequest {
    return CreateUserRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<CreateUserRequest>, I>>(object: I): CreateUserRequest {
    const message = createBaseCreateUserRequest();
    message.uid = object.uid ?? "";
    message.password = object.password ?? "";
    message.role = object.role ?? 0;
    return message;
  },
};

function createBaseCheckPasswordRequest(): CheckPasswordRequest {
  return { uid: "", password: "" };
}

export const CheckPasswordRequest = {
  encode(message: CheckPasswordRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CheckPasswordRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCheckPasswordRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.password = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): CheckPasswordRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      password: isSet(object.password) ? String(object.password) : "",
    };
  },

  toJSON(message: CheckPasswordRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<CheckPasswordRequest>, I>>(base?: I): CheckPasswordRequest {
    return CheckPasswordRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<CheckPasswordRequest>, I>>(object: I): CheckPasswordRequest {
    const message = createBaseCheckPasswordRequest();
    message.uid = object.uid ?? "";
    message.password = object.password ?? "";
    return message;
  },
};

function createBaseChangeTrustEndDeviceRequest(): ChangeTrustEndDeviceRequest {
  return { uid: "", peerId: "", action: 0 };
}

export const ChangeTrustEndDeviceRequest = {
  encode(message: ChangeTrustEndDeviceRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.peerId !== "") {
      writer.uint32(18).string(message.peerId);
    }
    if (message.action !== 0) {
      writer.uint32(24).int32(message.action);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangeTrustEndDeviceRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseChangeTrustEndDeviceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.peerId = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.action = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ChangeTrustEndDeviceRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      peerId: isSet(object.peerId) ? String(object.peerId) : "",
      action: isSet(object.action) ? changeTrustEndDeviceRequest_ActionFromJSON(object.action) : 0,
    };
  },

  toJSON(message: ChangeTrustEndDeviceRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.peerId !== "") {
      obj.peerId = message.peerId;
    }
    if (message.action !== 0) {
      obj.action = changeTrustEndDeviceRequest_ActionToJSON(message.action);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ChangeTrustEndDeviceRequest>, I>>(base?: I): ChangeTrustEndDeviceRequest {
    return ChangeTrustEndDeviceRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ChangeTrustEndDeviceRequest>, I>>(object: I): ChangeTrustEndDeviceRequest {
    const message = createBaseChangeTrustEndDeviceRequest();
    message.uid = object.uid ?? "";
    message.peerId = object.peerId ?? "";
    message.action = object.action ?? 0;
    return message;
  },
};

function createBaseChangeTrustEndDeviceReply(): ChangeTrustEndDeviceReply {
  return {};
}

export const ChangeTrustEndDeviceReply = {
  encode(_: ChangeTrustEndDeviceReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangeTrustEndDeviceReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseChangeTrustEndDeviceReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): ChangeTrustEndDeviceReply {
    return {};
  },

  toJSON(_: ChangeTrustEndDeviceReply): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<ChangeTrustEndDeviceReply>, I>>(base?: I): ChangeTrustEndDeviceReply {
    return ChangeTrustEndDeviceReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ChangeTrustEndDeviceReply>, I>>(_: I): ChangeTrustEndDeviceReply {
    const message = createBaseChangeTrustEndDeviceReply();
    return message;
  },
};

function createBaseSetupHServerRequest(): SetupHServerRequest {
  return { originServer: "", boxName: "", username: "", password: "" };
}

export const SetupHServerRequest = {
  encode(message: SetupHServerRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.originServer !== "") {
      writer.uint32(10).string(message.originServer);
    }
    if (message.boxName !== "") {
      writer.uint32(18).string(message.boxName);
    }
    if (message.username !== "") {
      writer.uint32(26).string(message.username);
    }
    if (message.password !== "") {
      writer.uint32(34).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetupHServerRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetupHServerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.originServer = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.boxName = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.username = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.password = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetupHServerRequest {
    return {
      originServer: isSet(object.originServer) ? String(object.originServer) : "",
      boxName: isSet(object.boxName) ? String(object.boxName) : "",
      username: isSet(object.username) ? String(object.username) : "",
      password: isSet(object.password) ? String(object.password) : "",
    };
  },

  toJSON(message: SetupHServerRequest): unknown {
    const obj: any = {};
    if (message.originServer !== "") {
      obj.originServer = message.originServer;
    }
    if (message.boxName !== "") {
      obj.boxName = message.boxName;
    }
    if (message.username !== "") {
      obj.username = message.username;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetupHServerRequest>, I>>(base?: I): SetupHServerRequest {
    return SetupHServerRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SetupHServerRequest>, I>>(object: I): SetupHServerRequest {
    const message = createBaseSetupHServerRequest();
    message.originServer = object.originServer ?? "";
    message.boxName = object.boxName ?? "";
    message.username = object.username ?? "";
    message.password = object.password ?? "";
    return message;
  },
};

function createBaseSetupHServerReply(): SetupHServerReply {
  return { ok: false, reason: "" };
}

export const SetupHServerReply = {
  encode(message: SetupHServerReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ok === true) {
      writer.uint32(8).bool(message.ok);
    }
    if (message.reason !== "") {
      writer.uint32(18).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetupHServerReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetupHServerReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.ok = reader.bool();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.reason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetupHServerReply {
    return {
      ok: isSet(object.ok) ? Boolean(object.ok) : false,
      reason: isSet(object.reason) ? String(object.reason) : "",
    };
  },

  toJSON(message: SetupHServerReply): unknown {
    const obj: any = {};
    if (message.ok === true) {
      obj.ok = message.ok;
    }
    if (message.reason !== "") {
      obj.reason = message.reason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetupHServerReply>, I>>(base?: I): SetupHServerReply {
    return SetupHServerReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SetupHServerReply>, I>>(object: I): SetupHServerReply {
    const message = createBaseSetupHServerReply();
    message.ok = object.ok ?? false;
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseResetHServerRequest(): ResetHServerRequest {
  return { originServer: "" };
}

export const ResetHServerRequest = {
  encode(message: ResetHServerRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.originServer !== "") {
      writer.uint32(10).string(message.originServer);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ResetHServerRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseResetHServerRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.originServer = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ResetHServerRequest {
    return { originServer: isSet(object.originServer) ? String(object.originServer) : "" };
  },

  toJSON(message: ResetHServerRequest): unknown {
    const obj: any = {};
    if (message.originServer !== "") {
      obj.originServer = message.originServer;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ResetHServerRequest>, I>>(base?: I): ResetHServerRequest {
    return ResetHServerRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ResetHServerRequest>, I>>(object: I): ResetHServerRequest {
    const message = createBaseResetHServerRequest();
    message.originServer = object.originServer ?? "";
    return message;
  },
};

function createBaseResetHServerReply(): ResetHServerReply {
  return {};
}

export const ResetHServerReply = {
  encode(_: ResetHServerReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ResetHServerReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseResetHServerReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): ResetHServerReply {
    return {};
  },

  toJSON(_: ResetHServerReply): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<ResetHServerReply>, I>>(base?: I): ResetHServerReply {
    return ResetHServerReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ResetHServerReply>, I>>(_: I): ResetHServerReply {
    const message = createBaseResetHServerReply();
    return message;
  },
};

function createBasePeersInfo(): PeersInfo {
  return { peerInfo: "" };
}

export const PeersInfo = {
  encode(message: PeersInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.peerInfo !== "") {
      writer.uint32(10).string(message.peerInfo);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PeersInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePeersInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.peerInfo = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): PeersInfo {
    return { peerInfo: isSet(object.peerInfo) ? String(object.peerInfo) : "" };
  },

  toJSON(message: PeersInfo): unknown {
    const obj: any = {};
    if (message.peerInfo !== "") {
      obj.peerInfo = message.peerInfo;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<PeersInfo>, I>>(base?: I): PeersInfo {
    return PeersInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<PeersInfo>, I>>(object: I): PeersInfo {
    const message = createBasePeersInfo();
    message.peerInfo = object.peerInfo ?? "";
    return message;
  },
};

function createBaseGetPasswordHashRequest(): GetPasswordHashRequest {
  return { uid: "" };
}

export const GetPasswordHashRequest = {
  encode(message: GetPasswordHashRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetPasswordHashRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetPasswordHashRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetPasswordHashRequest {
    return { uid: isSet(object.uid) ? String(object.uid) : "" };
  },

  toJSON(message: GetPasswordHashRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetPasswordHashRequest>, I>>(base?: I): GetPasswordHashRequest {
    return GetPasswordHashRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetPasswordHashRequest>, I>>(object: I): GetPasswordHashRequest {
    const message = createBaseGetPasswordHashRequest();
    message.uid = object.uid ?? "";
    return message;
  },
};

function createBaseGetPasswordHashResponse(): GetPasswordHashResponse {
  return { passwordHash: "" };
}

export const GetPasswordHashResponse = {
  encode(message: GetPasswordHashResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.passwordHash !== "") {
      writer.uint32(10).string(message.passwordHash);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetPasswordHashResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetPasswordHashResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.passwordHash = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetPasswordHashResponse {
    return { passwordHash: isSet(object.passwordHash) ? String(object.passwordHash) : "" };
  },

  toJSON(message: GetPasswordHashResponse): unknown {
    const obj: any = {};
    if (message.passwordHash !== "") {
      obj.passwordHash = message.passwordHash;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetPasswordHashResponse>, I>>(base?: I): GetPasswordHashResponse {
    return GetPasswordHashResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetPasswordHashResponse>, I>>(object: I): GetPasswordHashResponse {
    const message = createBaseGetPasswordHashResponse();
    message.passwordHash = object.passwordHash ?? "";
    return message;
  },
};

function createBaseSetPasswordHashRequest(): SetPasswordHashRequest {
  return { uid: "", passwordHash: "" };
}

export const SetPasswordHashRequest = {
  encode(message: SetPasswordHashRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.passwordHash !== "") {
      writer.uint32(18).string(message.passwordHash);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetPasswordHashRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetPasswordHashRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.passwordHash = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetPasswordHashRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      passwordHash: isSet(object.passwordHash) ? String(object.passwordHash) : "",
    };
  },

  toJSON(message: SetPasswordHashRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.passwordHash !== "") {
      obj.passwordHash = message.passwordHash;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetPasswordHashRequest>, I>>(base?: I): SetPasswordHashRequest {
    return SetPasswordHashRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SetPasswordHashRequest>, I>>(object: I): SetPasswordHashRequest {
    const message = createBaseSetPasswordHashRequest();
    message.uid = object.uid ?? "";
    message.passwordHash = object.passwordHash ?? "";
    return message;
  },
};

function createBaseSetRelayRequest(): SetRelayRequest {
  return { relayAddress: "" };
}

export const SetRelayRequest = {
  encode(message: SetRelayRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.relayAddress !== "") {
      writer.uint32(10).string(message.relayAddress);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetRelayRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetRelayRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.relayAddress = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetRelayRequest {
    return { relayAddress: isSet(object.relayAddress) ? String(object.relayAddress) : "" };
  },

  toJSON(message: SetRelayRequest): unknown {
    const obj: any = {};
    if (message.relayAddress !== "") {
      obj.relayAddress = message.relayAddress;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetRelayRequest>, I>>(base?: I): SetRelayRequest {
    return SetRelayRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SetRelayRequest>, I>>(object: I): SetRelayRequest {
    const message = createBaseSetRelayRequest();
    message.relayAddress = object.relayAddress ?? "";
    return message;
  },
};

function createBaseSetRelaysRequest(): SetRelaysRequest {
  return { relayAddresses: [] };
}

export const SetRelaysRequest = {
  encode(message: SetRelaysRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.relayAddresses) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetRelaysRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetRelaysRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.relayAddresses.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetRelaysRequest {
    return {
      relayAddresses: Array.isArray(object?.relayAddresses) ? object.relayAddresses.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: SetRelaysRequest): unknown {
    const obj: any = {};
    if (message.relayAddresses?.length) {
      obj.relayAddresses = message.relayAddresses;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetRelaysRequest>, I>>(base?: I): SetRelaysRequest {
    return SetRelaysRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SetRelaysRequest>, I>>(object: I): SetRelaysRequest {
    const message = createBaseSetRelaysRequest();
    message.relayAddresses = object.relayAddresses?.map((e) => e) || [];
    return message;
  },
};

function createBaseAllowProxyItRequest(): AllowProxyItRequest {
  return { peerId: "", disable: false };
}

export const AllowProxyItRequest = {
  encode(message: AllowProxyItRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.peerId !== "") {
      writer.uint32(10).string(message.peerId);
    }
    if (message.disable === true) {
      writer.uint32(16).bool(message.disable);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AllowProxyItRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAllowProxyItRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.peerId = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.disable = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AllowProxyItRequest {
    return {
      peerId: isSet(object.peerId) ? String(object.peerId) : "",
      disable: isSet(object.disable) ? Boolean(object.disable) : false,
    };
  },

  toJSON(message: AllowProxyItRequest): unknown {
    const obj: any = {};
    if (message.peerId !== "") {
      obj.peerId = message.peerId;
    }
    if (message.disable === true) {
      obj.disable = message.disable;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AllowProxyItRequest>, I>>(base?: I): AllowProxyItRequest {
    return AllowProxyItRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AllowProxyItRequest>, I>>(object: I): AllowProxyItRequest {
    const message = createBaseAllowProxyItRequest();
    message.peerId = object.peerId ?? "";
    message.disable = object.disable ?? false;
    return message;
  },
};

function createBaseAllowProxyItResponse(): AllowProxyItResponse {
  return {};
}

export const AllowProxyItResponse = {
  encode(_: AllowProxyItResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AllowProxyItResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAllowProxyItResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): AllowProxyItResponse {
    return {};
  },

  toJSON(_: AllowProxyItResponse): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<AllowProxyItResponse>, I>>(base?: I): AllowProxyItResponse {
    return AllowProxyItResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AllowProxyItResponse>, I>>(_: I): AllowProxyItResponse {
    const message = createBaseAllowProxyItResponse();
    return message;
  },
};

function createBaseAddress(): Address {
  return { multiaddr: "", ttl: 0 };
}

export const Address = {
  encode(message: Address, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.multiaddr !== "") {
      writer.uint32(10).string(message.multiaddr);
    }
    if (message.ttl !== 0) {
      writer.uint32(16).int64(message.ttl);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddress();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.multiaddr = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.ttl = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Address {
    return {
      multiaddr: isSet(object.multiaddr) ? String(object.multiaddr) : "",
      ttl: isSet(object.ttl) ? Number(object.ttl) : 0,
    };
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    if (message.multiaddr !== "") {
      obj.multiaddr = message.multiaddr;
    }
    if (message.ttl !== 0) {
      obj.ttl = Math.round(message.ttl);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<Address>, I>>(base?: I): Address {
    return Address.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<Address>, I>>(object: I): Address {
    const message = createBaseAddress();
    message.multiaddr = object.multiaddr ?? "";
    message.ttl = object.ttl ?? 0;
    return message;
  },
};

function createBaseAddAddressRequest(): AddAddressRequest {
  return { addresses: [] };
}

export const AddAddressRequest = {
  encode(message: AddAddressRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.addresses) {
      Address.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AddAddressRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddAddressRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.addresses.push(Address.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AddAddressRequest {
    return { addresses: Array.isArray(object?.addresses) ? object.addresses.map((e: any) => Address.fromJSON(e)) : [] };
  },

  toJSON(message: AddAddressRequest): unknown {
    const obj: any = {};
    if (message.addresses?.length) {
      obj.addresses = message.addresses.map((e) => Address.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AddAddressRequest>, I>>(base?: I): AddAddressRequest {
    return AddAddressRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AddAddressRequest>, I>>(object: I): AddAddressRequest {
    const message = createBaseAddAddressRequest();
    message.addresses = object.addresses?.map((e) => Address.fromPartial(e)) || [];
    return message;
  },
};

function createBaseAddAddressResponse(): AddAddressResponse {
  return {};
}

export const AddAddressResponse = {
  encode(_: AddAddressResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AddAddressResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddAddressResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): AddAddressResponse {
    return {};
  },

  toJSON(_: AddAddressResponse): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<AddAddressResponse>, I>>(base?: I): AddAddressResponse {
    return AddAddressResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AddAddressResponse>, I>>(_: I): AddAddressResponse {
    const message = createBaseAddAddressResponse();
    return message;
  },
};

export interface HPortalSys {
  /** 查询HServer当前状态 */
  QueryHServerInfo(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<HServerInfo>;
  /** 查询所有UID */
  ListUsers(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<ListUsersReply>;
  /** 创建用户信息 */
  CreateUser(
    request: DeepPartial<CreateUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 删除用户信息 */
  DeleteUser(
    request: DeepPartial<DeleteUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 修改新的密码 */
  ResetPassword(
    request: DeepPartial<ResetPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 校验用户密码是否正确 */
  CheckPassword(
    request: DeepPartial<CheckPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 获取用户密码Hash（供备份还原使用） */
  GetPasswordHash(
    request: DeepPartial<GetPasswordHashRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetPasswordHashResponse>;
  /** 设置用户密码Hash（供备份还原使用） */
  SetPasswordHash(
    request: DeepPartial<SetPasswordHashRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 根据用户uid查询用户信息 */
  QueryRole(request: DeepPartial<UserID>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<QueryRoleReply>;
  /** 修改指定uid的用户角色 */
  ChangeRole(
    request: DeepPartial<ChangeRoleReqeust>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 添加或删除受信任设备 */
  ChangeTrustEndDevice(
    request: DeepPartial<ChangeTrustEndDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ChangeTrustEndDeviceReply>;
  /** 根据UID返回所有的设备列表 */
  ListEndDevices(
    request: DeepPartial<ListEndDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ListEndDeviceReply>;
  /** 获取remotesocks服务器地址 */
  RemoteSocks(
    request: DeepPartial<RemoteSocksRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemoteSocksReply>;
  /** 仅在盒子未初始化时，可以被调用。 */
  SetupHServer(
    request: DeepPartial<SetupHServerRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SetupHServerReply>;
  /**
   * 重置盒子
   * 1. 向Origin请求释放盒子名下的所有域名
   * 2. 清除本地的box.name
   * 3. 进入为初始化状态
   */
  ResetHServer(
    request: DeepPartial<ResetHServerRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ResetHServerReply>;
  /**
   * 注册盒子服务
   * 任何原因导致此调用结束时，都会使此服务注销。(比如hportal重启)  // 调用者需要自行重新注册
   */
  RegisterBoxService(
    request: DeepPartial<RegisterBoxServiceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<RegisterBoxServiceReply>;
  /** 通知某个盒子服务发生变化 */
  EmitBoxServiceChanged(
    request: DeepPartial<EmitBoxServiceChangedRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 通过远端IP地址和服务注册的IP地址查询peer信息 */
  QueryBoxServicePeerCred(
    request: DeepPartial<QueryBoxServicePeerCredRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<QueryBoxServicePeerCredResponse>;
  /**
   * 允许目标peer.ID挂载到自身,成功挂载后，其他节点可以通过hserver代理到目标peer.ID
   * hserver重启后配置会丢失，调用者需要自行重新请求
   */
  AllowProxyIt(
    request: DeepPartial<AllowProxyItRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AllowProxyItResponse>;
  /**
   * 添加HServer对外广播的地址
   * 通过此接口最多可以添加10个地址
   */
  AddAddress(
    request: DeepPartial<AddAddressRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AddAddressResponse>;
}

export class HPortalSysClientImpl implements HPortalSys {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.QueryHServerInfo = this.QueryHServerInfo.bind(this);
    this.ListUsers = this.ListUsers.bind(this);
    this.CreateUser = this.CreateUser.bind(this);
    this.DeleteUser = this.DeleteUser.bind(this);
    this.ResetPassword = this.ResetPassword.bind(this);
    this.CheckPassword = this.CheckPassword.bind(this);
    this.GetPasswordHash = this.GetPasswordHash.bind(this);
    this.SetPasswordHash = this.SetPasswordHash.bind(this);
    this.QueryRole = this.QueryRole.bind(this);
    this.ChangeRole = this.ChangeRole.bind(this);
    this.ChangeTrustEndDevice = this.ChangeTrustEndDevice.bind(this);
    this.ListEndDevices = this.ListEndDevices.bind(this);
    this.RemoteSocks = this.RemoteSocks.bind(this);
    this.SetupHServer = this.SetupHServer.bind(this);
    this.ResetHServer = this.ResetHServer.bind(this);
    this.RegisterBoxService = this.RegisterBoxService.bind(this);
    this.EmitBoxServiceChanged = this.EmitBoxServiceChanged.bind(this);
    this.QueryBoxServicePeerCred = this.QueryBoxServicePeerCred.bind(this);
    this.AllowProxyIt = this.AllowProxyIt.bind(this);
    this.AddAddress = this.AddAddress.bind(this);
  }

  QueryHServerInfo(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<HServerInfo> {
    return this.rpc.unary(HPortalSysQueryHServerInfoDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  ListUsers(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<ListUsersReply> {
    return this.rpc.unary(HPortalSysListUsersDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  CreateUser(
    request: DeepPartial<CreateUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(HPortalSysCreateUserDesc, CreateUserRequest.fromPartial(request), metadata, abortSignal);
  }

  DeleteUser(
    request: DeepPartial<DeleteUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(HPortalSysDeleteUserDesc, DeleteUserRequest.fromPartial(request), metadata, abortSignal);
  }

  ResetPassword(
    request: DeepPartial<ResetPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      HPortalSysResetPasswordDesc,
      ResetPasswordRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  CheckPassword(
    request: DeepPartial<CheckPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      HPortalSysCheckPasswordDesc,
      CheckPasswordRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  GetPasswordHash(
    request: DeepPartial<GetPasswordHashRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetPasswordHashResponse> {
    return this.rpc.unary(
      HPortalSysGetPasswordHashDesc,
      GetPasswordHashRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  SetPasswordHash(
    request: DeepPartial<SetPasswordHashRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      HPortalSysSetPasswordHashDesc,
      SetPasswordHashRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  QueryRole(
    request: DeepPartial<UserID>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<QueryRoleReply> {
    return this.rpc.unary(HPortalSysQueryRoleDesc, UserID.fromPartial(request), metadata, abortSignal);
  }

  ChangeRole(
    request: DeepPartial<ChangeRoleReqeust>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(HPortalSysChangeRoleDesc, ChangeRoleReqeust.fromPartial(request), metadata, abortSignal);
  }

  ChangeTrustEndDevice(
    request: DeepPartial<ChangeTrustEndDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ChangeTrustEndDeviceReply> {
    return this.rpc.unary(
      HPortalSysChangeTrustEndDeviceDesc,
      ChangeTrustEndDeviceRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  ListEndDevices(
    request: DeepPartial<ListEndDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ListEndDeviceReply> {
    return this.rpc.unary(
      HPortalSysListEndDevicesDesc,
      ListEndDeviceRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  RemoteSocks(
    request: DeepPartial<RemoteSocksRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemoteSocksReply> {
    return this.rpc.unary(HPortalSysRemoteSocksDesc, RemoteSocksRequest.fromPartial(request), metadata, abortSignal);
  }

  SetupHServer(
    request: DeepPartial<SetupHServerRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SetupHServerReply> {
    return this.rpc.unary(HPortalSysSetupHServerDesc, SetupHServerRequest.fromPartial(request), metadata, abortSignal);
  }

  ResetHServer(
    request: DeepPartial<ResetHServerRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ResetHServerReply> {
    return this.rpc.unary(HPortalSysResetHServerDesc, ResetHServerRequest.fromPartial(request), metadata, abortSignal);
  }

  RegisterBoxService(
    request: DeepPartial<RegisterBoxServiceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<RegisterBoxServiceReply> {
    return this.rpc.invoke(
      HPortalSysRegisterBoxServiceDesc,
      RegisterBoxServiceRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  EmitBoxServiceChanged(
    request: DeepPartial<EmitBoxServiceChangedRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      HPortalSysEmitBoxServiceChangedDesc,
      EmitBoxServiceChangedRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  QueryBoxServicePeerCred(
    request: DeepPartial<QueryBoxServicePeerCredRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<QueryBoxServicePeerCredResponse> {
    return this.rpc.unary(
      HPortalSysQueryBoxServicePeerCredDesc,
      QueryBoxServicePeerCredRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  AllowProxyIt(
    request: DeepPartial<AllowProxyItRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AllowProxyItResponse> {
    return this.rpc.unary(HPortalSysAllowProxyItDesc, AllowProxyItRequest.fromPartial(request), metadata, abortSignal);
  }

  AddAddress(
    request: DeepPartial<AddAddressRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AddAddressResponse> {
    return this.rpc.unary(HPortalSysAddAddressDesc, AddAddressRequest.fromPartial(request), metadata, abortSignal);
  }
}

export const HPortalSysDesc = { serviceName: "cloud.lazycat.apis.sys.HPortalSys" };

export const HPortalSysQueryHServerInfoDesc: UnaryMethodDefinitionish = {
  methodName: "QueryHServerInfo",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = HServerInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysListUsersDesc: UnaryMethodDefinitionish = {
  methodName: "ListUsers",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ListUsersReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysCreateUserDesc: UnaryMethodDefinitionish = {
  methodName: "CreateUser",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return CreateUserRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysDeleteUserDesc: UnaryMethodDefinitionish = {
  methodName: "DeleteUser",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return DeleteUserRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysResetPasswordDesc: UnaryMethodDefinitionish = {
  methodName: "ResetPassword",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ResetPasswordRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysCheckPasswordDesc: UnaryMethodDefinitionish = {
  methodName: "CheckPassword",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return CheckPasswordRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysGetPasswordHashDesc: UnaryMethodDefinitionish = {
  methodName: "GetPasswordHash",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetPasswordHashRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = GetPasswordHashResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysSetPasswordHashDesc: UnaryMethodDefinitionish = {
  methodName: "SetPasswordHash",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SetPasswordHashRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysQueryRoleDesc: UnaryMethodDefinitionish = {
  methodName: "QueryRole",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserID.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = QueryRoleReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysChangeRoleDesc: UnaryMethodDefinitionish = {
  methodName: "ChangeRole",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangeRoleReqeust.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysChangeTrustEndDeviceDesc: UnaryMethodDefinitionish = {
  methodName: "ChangeTrustEndDevice",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangeTrustEndDeviceRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ChangeTrustEndDeviceReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysListEndDevicesDesc: UnaryMethodDefinitionish = {
  methodName: "ListEndDevices",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ListEndDeviceRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ListEndDeviceReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysRemoteSocksDesc: UnaryMethodDefinitionish = {
  methodName: "RemoteSocks",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return RemoteSocksRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = RemoteSocksReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysSetupHServerDesc: UnaryMethodDefinitionish = {
  methodName: "SetupHServer",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SetupHServerRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SetupHServerReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysResetHServerDesc: UnaryMethodDefinitionish = {
  methodName: "ResetHServer",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ResetHServerRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ResetHServerReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysRegisterBoxServiceDesc: UnaryMethodDefinitionish = {
  methodName: "RegisterBoxService",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return RegisterBoxServiceRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = RegisterBoxServiceReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysEmitBoxServiceChangedDesc: UnaryMethodDefinitionish = {
  methodName: "EmitBoxServiceChanged",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return EmitBoxServiceChangedRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysQueryBoxServicePeerCredDesc: UnaryMethodDefinitionish = {
  methodName: "QueryBoxServicePeerCred",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return QueryBoxServicePeerCredRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = QueryBoxServicePeerCredResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysAllowProxyItDesc: UnaryMethodDefinitionish = {
  methodName: "AllowProxyIt",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return AllowProxyItRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = AllowProxyItResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const HPortalSysAddAddressDesc: UnaryMethodDefinitionish = {
  methodName: "AddAddress",
  service: HPortalSysDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return AddAddressRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = AddAddressResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = (t.seconds || 0) * 1_000;
  millis += (t.nanos || 0) / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === "string") {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
