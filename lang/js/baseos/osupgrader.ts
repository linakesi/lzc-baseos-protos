/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Empty } from "../google/protobuf/empty";

export interface OSUpgraderDownloadEntry {
  url: string;
  size: number;
  /** 不传入，只在 GetInfo 的时候可能会获取到 */
  diffSize?: number | undefined;
  hash: string;
}

export interface OSUpgraderDownloadRequest {
  version: string;
  baseosVersion: string;
  entries: OSUpgraderDownloadEntry[];
}

export interface OSUpgraderInfo {
  /** 状态 */
  state: OSUpgraderInfo_State;
  /** 当前 bsid */
  bsid: string;
  /** 当前 lzcos 版本 */
  currentVersion: string;
  /** 当前 baseos 版本 */
  currentBaseosVersion: string;
  /** 正在下载的 lzcos 版本 */
  downloadVersion: string;
  /** 正在下载的 baseos 版本 */
  downloadBaseosVersion: string;
  /** 已下载的大小（包含所有包）（字节） */
  downloadComplete: number;
  /** 总需要下载的大小（包含所有包）（字节） */
  downloadTotal: number;
  /** 预期的所有下载项目 */
  downloadEntries: OSUpgraderDownloadEntry[];
  /** 错误信息（在没有错误时该值为空） */
  error: string;
}

export enum OSUpgraderInfo_State {
  IDLE = 0,
  DOWNLOADING = 1,
  DOWNLOAD_PAUSED = 2,
  DOWNLOAD_COMPLETED = 3,
  INSTALLING = 4,
  ERROR = 5,
  UNRECOGNIZED = -1,
}

export function oSUpgraderInfo_StateFromJSON(object: any): OSUpgraderInfo_State {
  switch (object) {
    case 0:
    case "IDLE":
      return OSUpgraderInfo_State.IDLE;
    case 1:
    case "DOWNLOADING":
      return OSUpgraderInfo_State.DOWNLOADING;
    case 2:
    case "DOWNLOAD_PAUSED":
      return OSUpgraderInfo_State.DOWNLOAD_PAUSED;
    case 3:
    case "DOWNLOAD_COMPLETED":
      return OSUpgraderInfo_State.DOWNLOAD_COMPLETED;
    case 4:
    case "INSTALLING":
      return OSUpgraderInfo_State.INSTALLING;
    case 5:
    case "ERROR":
      return OSUpgraderInfo_State.ERROR;
    case -1:
    case "UNRECOGNIZED":
    default:
      return OSUpgraderInfo_State.UNRECOGNIZED;
  }
}

export function oSUpgraderInfo_StateToJSON(object: OSUpgraderInfo_State): string {
  switch (object) {
    case OSUpgraderInfo_State.IDLE:
      return "IDLE";
    case OSUpgraderInfo_State.DOWNLOADING:
      return "DOWNLOADING";
    case OSUpgraderInfo_State.DOWNLOAD_PAUSED:
      return "DOWNLOAD_PAUSED";
    case OSUpgraderInfo_State.DOWNLOAD_COMPLETED:
      return "DOWNLOAD_COMPLETED";
    case OSUpgraderInfo_State.INSTALLING:
      return "INSTALLING";
    case OSUpgraderInfo_State.ERROR:
      return "ERROR";
    case OSUpgraderInfo_State.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseOSUpgraderDownloadEntry(): OSUpgraderDownloadEntry {
  return { url: "", size: 0, diffSize: undefined, hash: "" };
}

export const OSUpgraderDownloadEntry = {
  encode(message: OSUpgraderDownloadEntry, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.url !== "") {
      writer.uint32(10).string(message.url);
    }
    if (message.size !== 0) {
      writer.uint32(16).int64(message.size);
    }
    if (message.diffSize !== undefined) {
      writer.uint32(32).int64(message.diffSize);
    }
    if (message.hash !== "") {
      writer.uint32(26).string(message.hash);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): OSUpgraderDownloadEntry {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseOSUpgraderDownloadEntry();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.url = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.size = longToNumber(reader.int64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.diffSize = longToNumber(reader.int64() as Long);
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.hash = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): OSUpgraderDownloadEntry {
    return {
      url: isSet(object.url) ? String(object.url) : "",
      size: isSet(object.size) ? Number(object.size) : 0,
      diffSize: isSet(object.diffSize) ? Number(object.diffSize) : undefined,
      hash: isSet(object.hash) ? String(object.hash) : "",
    };
  },

  toJSON(message: OSUpgraderDownloadEntry): unknown {
    const obj: any = {};
    if (message.url !== "") {
      obj.url = message.url;
    }
    if (message.size !== 0) {
      obj.size = Math.round(message.size);
    }
    if (message.diffSize !== undefined) {
      obj.diffSize = Math.round(message.diffSize);
    }
    if (message.hash !== "") {
      obj.hash = message.hash;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<OSUpgraderDownloadEntry>, I>>(base?: I): OSUpgraderDownloadEntry {
    return OSUpgraderDownloadEntry.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<OSUpgraderDownloadEntry>, I>>(object: I): OSUpgraderDownloadEntry {
    const message = createBaseOSUpgraderDownloadEntry();
    message.url = object.url ?? "";
    message.size = object.size ?? 0;
    message.diffSize = object.diffSize ?? undefined;
    message.hash = object.hash ?? "";
    return message;
  },
};

function createBaseOSUpgraderDownloadRequest(): OSUpgraderDownloadRequest {
  return { version: "", baseosVersion: "", entries: [] };
}

export const OSUpgraderDownloadRequest = {
  encode(message: OSUpgraderDownloadRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== "") {
      writer.uint32(10).string(message.version);
    }
    if (message.baseosVersion !== "") {
      writer.uint32(26).string(message.baseosVersion);
    }
    for (const v of message.entries) {
      OSUpgraderDownloadEntry.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): OSUpgraderDownloadRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseOSUpgraderDownloadRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.version = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.baseosVersion = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.entries.push(OSUpgraderDownloadEntry.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): OSUpgraderDownloadRequest {
    return {
      version: isSet(object.version) ? String(object.version) : "",
      baseosVersion: isSet(object.baseosVersion) ? String(object.baseosVersion) : "",
      entries: Array.isArray(object?.entries)
        ? object.entries.map((e: any) => OSUpgraderDownloadEntry.fromJSON(e))
        : [],
    };
  },

  toJSON(message: OSUpgraderDownloadRequest): unknown {
    const obj: any = {};
    if (message.version !== "") {
      obj.version = message.version;
    }
    if (message.baseosVersion !== "") {
      obj.baseosVersion = message.baseosVersion;
    }
    if (message.entries?.length) {
      obj.entries = message.entries.map((e) => OSUpgraderDownloadEntry.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<OSUpgraderDownloadRequest>, I>>(base?: I): OSUpgraderDownloadRequest {
    return OSUpgraderDownloadRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<OSUpgraderDownloadRequest>, I>>(object: I): OSUpgraderDownloadRequest {
    const message = createBaseOSUpgraderDownloadRequest();
    message.version = object.version ?? "";
    message.baseosVersion = object.baseosVersion ?? "";
    message.entries = object.entries?.map((e) => OSUpgraderDownloadEntry.fromPartial(e)) || [];
    return message;
  },
};

function createBaseOSUpgraderInfo(): OSUpgraderInfo {
  return {
    state: 0,
    bsid: "",
    currentVersion: "",
    currentBaseosVersion: "",
    downloadVersion: "",
    downloadBaseosVersion: "",
    downloadComplete: 0,
    downloadTotal: 0,
    downloadEntries: [],
    error: "",
  };
}

export const OSUpgraderInfo = {
  encode(message: OSUpgraderInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.state !== 0) {
      writer.uint32(8).int32(message.state);
    }
    if (message.bsid !== "") {
      writer.uint32(82).string(message.bsid);
    }
    if (message.currentVersion !== "") {
      writer.uint32(18).string(message.currentVersion);
    }
    if (message.currentBaseosVersion !== "") {
      writer.uint32(66).string(message.currentBaseosVersion);
    }
    if (message.downloadVersion !== "") {
      writer.uint32(26).string(message.downloadVersion);
    }
    if (message.downloadBaseosVersion !== "") {
      writer.uint32(74).string(message.downloadBaseosVersion);
    }
    if (message.downloadComplete !== 0) {
      writer.uint32(32).int64(message.downloadComplete);
    }
    if (message.downloadTotal !== 0) {
      writer.uint32(40).int64(message.downloadTotal);
    }
    for (const v of message.downloadEntries) {
      OSUpgraderDownloadEntry.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    if (message.error !== "") {
      writer.uint32(58).string(message.error);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): OSUpgraderInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseOSUpgraderInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.state = reader.int32() as any;
          continue;
        case 10:
          if (tag !== 82) {
            break;
          }

          message.bsid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.currentVersion = reader.string();
          continue;
        case 8:
          if (tag !== 66) {
            break;
          }

          message.currentBaseosVersion = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.downloadVersion = reader.string();
          continue;
        case 9:
          if (tag !== 74) {
            break;
          }

          message.downloadBaseosVersion = reader.string();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.downloadComplete = longToNumber(reader.int64() as Long);
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.downloadTotal = longToNumber(reader.int64() as Long);
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.downloadEntries.push(OSUpgraderDownloadEntry.decode(reader, reader.uint32()));
          continue;
        case 7:
          if (tag !== 58) {
            break;
          }

          message.error = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): OSUpgraderInfo {
    return {
      state: isSet(object.state) ? oSUpgraderInfo_StateFromJSON(object.state) : 0,
      bsid: isSet(object.bsid) ? String(object.bsid) : "",
      currentVersion: isSet(object.currentVersion) ? String(object.currentVersion) : "",
      currentBaseosVersion: isSet(object.currentBaseosVersion) ? String(object.currentBaseosVersion) : "",
      downloadVersion: isSet(object.downloadVersion) ? String(object.downloadVersion) : "",
      downloadBaseosVersion: isSet(object.downloadBaseosVersion) ? String(object.downloadBaseosVersion) : "",
      downloadComplete: isSet(object.downloadComplete) ? Number(object.downloadComplete) : 0,
      downloadTotal: isSet(object.downloadTotal) ? Number(object.downloadTotal) : 0,
      downloadEntries: Array.isArray(object?.downloadEntries)
        ? object.downloadEntries.map((e: any) => OSUpgraderDownloadEntry.fromJSON(e))
        : [],
      error: isSet(object.error) ? String(object.error) : "",
    };
  },

  toJSON(message: OSUpgraderInfo): unknown {
    const obj: any = {};
    if (message.state !== 0) {
      obj.state = oSUpgraderInfo_StateToJSON(message.state);
    }
    if (message.bsid !== "") {
      obj.bsid = message.bsid;
    }
    if (message.currentVersion !== "") {
      obj.currentVersion = message.currentVersion;
    }
    if (message.currentBaseosVersion !== "") {
      obj.currentBaseosVersion = message.currentBaseosVersion;
    }
    if (message.downloadVersion !== "") {
      obj.downloadVersion = message.downloadVersion;
    }
    if (message.downloadBaseosVersion !== "") {
      obj.downloadBaseosVersion = message.downloadBaseosVersion;
    }
    if (message.downloadComplete !== 0) {
      obj.downloadComplete = Math.round(message.downloadComplete);
    }
    if (message.downloadTotal !== 0) {
      obj.downloadTotal = Math.round(message.downloadTotal);
    }
    if (message.downloadEntries?.length) {
      obj.downloadEntries = message.downloadEntries.map((e) => OSUpgraderDownloadEntry.toJSON(e));
    }
    if (message.error !== "") {
      obj.error = message.error;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<OSUpgraderInfo>, I>>(base?: I): OSUpgraderInfo {
    return OSUpgraderInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<OSUpgraderInfo>, I>>(object: I): OSUpgraderInfo {
    const message = createBaseOSUpgraderInfo();
    message.state = object.state ?? 0;
    message.bsid = object.bsid ?? "";
    message.currentVersion = object.currentVersion ?? "";
    message.currentBaseosVersion = object.currentBaseosVersion ?? "";
    message.downloadVersion = object.downloadVersion ?? "";
    message.downloadBaseosVersion = object.downloadBaseosVersion ?? "";
    message.downloadComplete = object.downloadComplete ?? 0;
    message.downloadTotal = object.downloadTotal ?? 0;
    message.downloadEntries = object.downloadEntries?.map((e) => OSUpgraderDownloadEntry.fromPartial(e)) || [];
    message.error = object.error ?? "";
    return message;
  },
};

/** 新的 更新接口 */
export interface OSUpgraderService {
  Download(
    request: DeepPartial<OSUpgraderDownloadRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  PauseDownload(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  Install(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  GetInfo(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<OSUpgraderInfo>;
}

export class OSUpgraderServiceClientImpl implements OSUpgraderService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Download = this.Download.bind(this);
    this.PauseDownload = this.PauseDownload.bind(this);
    this.Install = this.Install.bind(this);
    this.GetInfo = this.GetInfo.bind(this);
  }

  Download(
    request: DeepPartial<OSUpgraderDownloadRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      OSUpgraderServiceDownloadDesc,
      OSUpgraderDownloadRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  PauseDownload(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgraderServicePauseDownloadDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Install(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(OSUpgraderServiceInstallDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  GetInfo(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<OSUpgraderInfo> {
    return this.rpc.unary(OSUpgraderServiceGetInfoDesc, Empty.fromPartial(request), metadata, abortSignal);
  }
}

export const OSUpgraderServiceDesc = { serviceName: "cloud.lazycat.boxservice.daemon.OSUpgraderService" };

export const OSUpgraderServiceDownloadDesc: UnaryMethodDefinitionish = {
  methodName: "Download",
  service: OSUpgraderServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return OSUpgraderDownloadRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgraderServicePauseDownloadDesc: UnaryMethodDefinitionish = {
  methodName: "PauseDownload",
  service: OSUpgraderServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgraderServiceInstallDesc: UnaryMethodDefinitionish = {
  methodName: "Install",
  service: OSUpgraderServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const OSUpgraderServiceGetInfoDesc: UnaryMethodDefinitionish = {
  methodName: "GetInfo",
  service: OSUpgraderServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = OSUpgraderInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
