package baseos

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"time"

	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/peer"
)

var TODO_REMOVE_IS_IN_LZCAPP = false

type HServerClient struct {
	HPortalSysClient
	conn *grpc.ClientConn
}

func (c HServerClient) Close() error { return c.conn.Close() }

func NewHServerClient() (*HServerClient, error) {
	var SocketPath = "/run/lzc-sys/portal-server.socket"
	if TODO_REMOVE_IS_IN_LZCAPP {
		SocketPath = "/lzcapp/run/sys/portal-server.socket"
	}
	conn, err := grpc.Dial("unix://"+SocketPath, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return &HServerClient{HPortalSysClient: NewHPortalSysClient(conn), conn: conn}, nil
}

type DaemonClients struct {
	FaultFixServiceClient
	OSUpgraderServiceClient
	PowerServiceClient

	conn *grpc.ClientConn
}

func (c DaemonClients) Close() error { return c.conn.Close() }

func NewDaemonClients() (*DaemonClients, error) {
	// 注意: lzcos/os-scripts/rootfs/lzcsys/bin/box-init 也有引用这个地址
	var SocketPath = "/run/lzc-sys/lzc-base-daemon.sock"
	conn, err := grpc.Dial("unix://"+SocketPath, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return &DaemonClients{
		conn:                    conn,
		FaultFixServiceClient:   NewFaultFixServiceClient(conn),
		OSUpgraderServiceClient: NewOSUpgraderServiceClient(conn),
		PowerServiceClient:      NewPowerServiceClient(conn),
	}, nil
}

func RegisterBoxService(hsrvC HPortalSysClient, req *RegisterBoxServiceRequest) error {
	s, err := hsrvC.RegisterBoxService(context.Background(), req)
	if err != nil {
		return err
	}
	_, err = s.Recv()
	if err != nil {
		return err
	}
	// second recv can only return if server exited
	_, err = s.Recv()
	return err
}

func KeepRegisterBoxService(hsrvC HPortalSysClient, req *RegisterBoxServiceRequest) {
	go func() {
		for {
			err := RegisterBoxService(hsrvC, req)
			if err != nil {
				fmt.Fprintf(os.Stderr, "注册%v失败:%v。1s后重试\n", req.ServiceName, err)
			}
			time.Sleep(time.Second)
		}
	}()
}

func DialerInfoFromBSRV(ctx context.Context, hsrvC HPortalSysClient, srvAddr net.Addr) (*QueryBoxServicePeerCredResponse, error) {
	grpcPeer, ok := peer.FromContext(ctx)
	if !ok {
		return nil, errors.New("failed to get grpc peer info from context")
	}

	network := grpcPeer.Addr.Network()
	switch network {
	case "tcp", "udp":
		break
	default:
		return nil, fmt.Errorf("%s transport doesn't support dialer info", network)
	}
	return hsrvC.QueryBoxServicePeerCred(ctx, &QueryBoxServicePeerCredRequest{
		RemoteAddr: grpcPeer.Addr.String(),
		LocalAddr:  srvAddr.String(),
		Protocol:   srvAddr.Network(),
	})
}
